[argvtype](../README.md) / [Exports](../modules.md) / Argument

# Class: Argument<Name, Desc, Type, Multiple, Optional, TypeName, Restrict\>

Argument

Holds the individual argument schema.

An argument is an positional value which is passed to the command line.

## Type parameters

Name | Type | Default |
:------ | :------ | :------ |
`Name` | *string* | - |
`Desc` | *string* \| *null* | *null* |
`Type` | - | *unknown* |
`Multiple` | *boolean* | *false* |
`Optional` | *boolean* | *false* |
`TypeName` | *string* \| *null* | *null* |
`Restrict` | *string*[] \| *null* | *null* |

## Table of contents

### Constructors

- [constructor](argument.md#constructor)

### Accessors

- [$](argument.md#$)

### Methods

- [coerce](argument.md#coerce)
- [default](argument.md#default)
- [description](argument.md#description)
- [enum](argument.md#enum)
- [name](argument.md#name)
- [optional](argument.md#optional)
- [required](argument.md#required)
- [type](argument.md#type)
- [variadic](argument.md#variadic)

## Constructors

### constructor

\+ **new Argument**<Name, Desc, Type, Multiple, Optional, TypeName, Restrict\>(`name`: Name): [*Argument*](argument.md)<Name, Desc, Type, Multiple, Optional, TypeName, Restrict\>

Create new Argument

#### Type parameters:

Name | Type | Default |
:------ | :------ | :------ |
`Name` | *string* | - |
`Desc` | *null* \| *string* | *null* |
`Type` | - | *unknown* |
`Multiple` | *boolean* | *false* |
`Optional` | *boolean* | *false* |
`TypeName` | *null* \| *string* | *null* |
`Restrict` | *null* \| *string*[] | *null* |

#### Parameters:

Name | Type | Description |
:------ | :------ | :------ |
`name` | Name | Unique name of argument.    |

**Returns:** [*Argument*](argument.md)<Name, Desc, Type, Multiple, Optional, TypeName, Restrict\>

Defined in: src/Arguments.ts:30

\+ **new Argument**<Name, Desc, Type, Multiple, Optional, TypeName, Restrict\>(`name`: Name, `description`: Desc, `cast`: *null* \| (`input`: Optional *extends* *true* ? *null* \| Restrict *extends* *null* ? *string* : Restrict *extends* T[] ? T : *never* : Restrict *extends* *null* ? *string* : Restrict *extends* T[] ? T : *never*) => Type, `variadic`: Multiple, `optional`: Optional, `typename`: TypeName, `enums`: Restrict, `defaults`: Type): [*Argument*](argument.md)<Name, Desc, Type, Multiple, Optional, TypeName, Restrict\>

Create new Argument

#### Type parameters:

Name | Type | Default |
:------ | :------ | :------ |
`Name` | *string* | - |
`Desc` | *null* \| *string* | *null* |
`Type` | - | *unknown* |
`Multiple` | *boolean* | *false* |
`Optional` | *boolean* | *false* |
`TypeName` | *null* \| *string* | *null* |
`Restrict` | *null* \| *string*[] | *null* |

#### Parameters:

Name | Type | Description |
:------ | :------ | :------ |
`name` | Name | Unique name of argument.   |
`description` | Desc | Short (1 line) description of the argument.   |
`cast` | *null* \| (`input`: Optional *extends* *true* ? *null* \| Restrict *extends* *null* ? *string* : Restrict *extends* T[] ? T : *never* : Restrict *extends* *null* ? *string* : Restrict *extends* T[] ? T : *never*) => Type | Function to cast input value to the output type. Throws class extending CatchableError on cast failure.   |
`variadic` | Multiple | Set to true to make the argument variadic.   |
`optional` | Optional | Set to true to make the argument optional.   |
`typename` | TypeName | Set the type name of the argument variable.   |
`enums` | Restrict | Restrictive list of strings that the argument can accept.   |
`defaults` | Type | The default value of the flag.    |

**Returns:** [*Argument*](argument.md)<Name, Desc, Type, Multiple, Optional, TypeName, Restrict\>

Defined in: src/Arguments.ts:39

## Accessors

### $

• get **$**(): *object*

Argument Configuration

**Returns:** *object*

Name | Type |
:------ | :------ |
`cast` | *null* \| (`input`: Optional *extends* *true* ? *null* \| Restrict *extends* *null* ? *string* : Restrict *extends* T[] ? T : *never* : Restrict *extends* *null* ? *string* : Restrict *extends* T[] ? T : *never*) => Type |
`default` | *symbol* \| Type |
`description` | Desc |
`enum` | Restrict |
`name` | Name |
`optional` | Optional |
`typename` | TypeName |
`variadic` | Multiple |

Defined in: src/Arguments.ts:170

## Methods

### coerce

▸ **coerce**<Type\>(`cast`: (`input`: Optional *extends* *true* ? *null* \| Restrict *extends* *null* ? *string* : Restrict *extends* T[] ? T : *never* : Restrict *extends* *null* ? *string* : Restrict *extends* T[] ? T : *never*) => Type): [*Argument*](argument.md)<Name, Desc, any, Multiple, Optional, TypeName, Restrict\>

Set the type and casting function of the argument.

#### Type parameters:

Name |
:------ |
`Type` |

#### Parameters:

Name | Type | Description |
:------ | :------ | :------ |
`cast` | (`input`: Optional *extends* *true* ? *null* \| Restrict *extends* *null* ? *string* : Restrict *extends* T[] ? T : *never* : Restrict *extends* *null* ? *string* : Restrict *extends* T[] ? T : *never*) => Type | Function to cast the input value to the output type. Throws class extending CatchableError on cast failure.    |

**Returns:** [*Argument*](argument.md)<Name, Desc, any, Multiple, Optional, TypeName, Restrict\>

Defined in: src/Arguments.ts:109

___

### default

▸ **default**<DefaultType\>(`defaults`: DefaultType): [*Argument*](argument.md)<Name, Desc, Type, Multiple, Optional, TypeName, Restrict\>

Set the default value of the argument.

#### Type parameters:

Name |
:------ |
`DefaultType` |

#### Parameters:

Name | Type | Description |
:------ | :------ | :------ |
`defaults` | DefaultType | The default value of the argument.    |

**Returns:** [*Argument*](argument.md)<Name, Desc, Type, Multiple, Optional, TypeName, Restrict\>

Defined in: src/Arguments.ts:164

___

### description

▸ **description**<Desc\>(`description`: Desc): [*Argument*](argument.md)<Name, Desc, Type \| *typeof* UNSET, Multiple, Optional, TypeName, Restrict\>

Set the description of the argument.

#### Type parameters:

Name | Type |
:------ | :------ |
`Desc` | *null* \| *string* |

#### Parameters:

Name | Type | Description |
:------ | :------ | :------ |
`description` | Desc | Short (1 line) description of the argument.    |

**Returns:** [*Argument*](argument.md)<Name, Desc, Type \| *typeof* UNSET, Multiple, Optional, TypeName, Restrict\>

Defined in: src/Arguments.ts:81

___

### enum

▸ **enum**<Restrict\>(...`restrict`: [...Restrict[]]): [*Argument*](argument.md)<Name, Desc, Restrict *extends* T[] ? T : *never*, Multiple, Optional, Name, [...Restrict[]]\>

Set the allowed values of the argument.

#### Type parameters:

Name | Type |
:------ | :------ |
`Restrict` | *string*[] |

#### Parameters:

Name | Type | Description |
:------ | :------ | :------ |
`...restrict` | [...Restrict[]] | Restrictive list of strings that the argument can accept.    |

**Returns:** [*Argument*](argument.md)<Name, Desc, Restrict *extends* T[] ? T : *never*, Multiple, Optional, Name, [...Restrict[]]\>

Defined in: src/Arguments.ts:90

___

### name

▸ **name**<Name\>(`name`: Name): [*Argument*](argument.md)<Name, Desc, Type \| *typeof* UNSET, Multiple, Optional, TypeName, Restrict\>

Set the name of the argument.

#### Type parameters:

Name | Type |
:------ | :------ |
`Name` | *string* |

#### Parameters:

Name | Type | Description |
:------ | :------ | :------ |
`name` | Name | Unique name of argument.    |

**Returns:** [*Argument*](argument.md)<Name, Desc, Type \| *typeof* UNSET, Multiple, Optional, TypeName, Restrict\>

Defined in: src/Arguments.ts:155

___

### optional

▸ **optional**(): [*Argument*](argument.md)<Name, Desc, Type, Multiple, *true*, TypeName, Restrict\>

Set argument to be optional.

**Returns:** [*Argument*](argument.md)<Name, Desc, Type, Multiple, *true*, TypeName, Restrict\>

Defined in: src/Arguments.ts:138

▸ **optional**<Optional\>(`optional`: Optional): [*Argument*](argument.md)<Name, Desc, Type, Multiple, Optional, TypeName, Restrict\>

Set argument optional state.

#### Type parameters:

Name | Type |
:------ | :------ |
`Optional` | *boolean* |

#### Parameters:

Name | Type | Description |
:------ | :------ | :------ |
`optional` | Optional | Set to true to make argument optional.    |

**Returns:** [*Argument*](argument.md)<Name, Desc, Type, Multiple, Optional, TypeName, Restrict\>

Defined in: src/Arguments.ts:144

___

### required

▸ **required**(): [*Argument*](argument.md)<Name, Desc, Type, Multiple, *false*, TypeName, Restrict\>

Set argument to be required.

**Returns:** [*Argument*](argument.md)<Name, Desc, Type, Multiple, *false*, TypeName, Restrict\>

Defined in: src/Arguments.ts:130

___

### type

▸ **type**<TypeName, Type\>(`name`: TypeName, `cast?`: (`input`: Optional *extends* *true* ? *null* \| Restrict *extends* *null* ? *string* : Restrict *extends* T[] ? T : *never* : Restrict *extends* *null* ? *string* : Restrict *extends* T[] ? T : *never*) => Type): [*Argument*](argument.md)<Name, Desc, Type, Multiple, Optional, TypeName, Restrict\>

Set the type, type name and casting function of the argument.

#### Type parameters:

Name | Type |
:------ | :------ |
`TypeName` | *null* \| *string* |
`Type` | - |

#### Parameters:

Name | Type | Description |
:------ | :------ | :------ |
`name` | TypeName | Set the type name of the argument variable.   |
`cast` | (`input`: Optional *extends* *true* ? *null* \| Restrict *extends* *null* ? *string* : Restrict *extends* T[] ? T : *never* : Restrict *extends* *null* ? *string* : Restrict *extends* T[] ? T : *never*) => Type | Function to cast the input value to the output type. Throws class extending CatchableError on cast failure.    |

**Returns:** [*Argument*](argument.md)<Name, Desc, Type, Multiple, Optional, TypeName, Restrict\>

Defined in: src/Arguments.ts:100

___

### variadic

▸ **variadic**(): [*Argument*](argument.md)<Name, Desc, Type, *true*, Optional, TypeName, Restrict\>

Set argument to be variadic.

**Returns:** [*Argument*](argument.md)<Name, Desc, Type, *true*, Optional, TypeName, Restrict\>

Defined in: src/Arguments.ts:116

▸ **variadic**<Multiple\>(`variadic`: Multiple): [*Argument*](argument.md)<Name, Desc, Type, Multiple, Optional, TypeName, Restrict\>

Set argument to be variadic.

#### Type parameters:

Name | Type |
:------ | :------ |
`Multiple` | *boolean* |

#### Parameters:

Name | Type | Description |
:------ | :------ | :------ |
`variadic` | Multiple | Set to true to make argument variadic.    |

**Returns:** [*Argument*](argument.md)<Name, Desc, Type, Multiple, Optional, TypeName, Restrict\>

Defined in: src/Arguments.ts:122
