[argvtype](../README.md) / [Exports](../modules.md) / Arguments

# Class: Arguments<Args, KeyArgs\>

Arguments

Holds entire positional argument schema.

Arguments contain all the positional arguments which can be passed to the command.

## Type parameters

Name | Type | Default |
:------ | :------ | :------ |
`Args` | [*Argument*](argument.md)<any, any, any, any, any, any, any\>[] | [] |
`KeyArgs` | *Record*<string, [*Argument*](argument.md)<any, any, any, any, any, any, any\>\> | {} |

## Table of contents

### Constructors

- [constructor](arguments.md#constructor)

### Accessors

- [$](arguments.md#$)

### Methods

- [addArg](arguments.md#addarg)
- [arg](arguments.md#arg)

## Constructors

### constructor

\+ **new Arguments**<Args, KeyArgs\>(): [*Arguments*](arguments.md)<Args, KeyArgs\>

Create new Arguments

#### Type parameters:

Name | Type | Default |
:------ | :------ | :------ |
`Args` | [*Argument*](argument.md)<any, any, any, any, any, any, any\>[] | [] |
`KeyArgs` | *Record*<string, [*Argument*](argument.md)<any, any, any, any, any, any, any\>\> | {} |

**Returns:** [*Arguments*](arguments.md)<Args, KeyArgs\>

Defined in: src/Arguments.ts:192

\+ **new Arguments**<Args, KeyArgs\>(`args`: [...Args[]], `keyargs`: KeyArgs): [*Arguments*](arguments.md)<Args, KeyArgs\>

Create new Arguments

#### Type parameters:

Name | Type | Default |
:------ | :------ | :------ |
`Args` | [*Argument*](argument.md)<any, any, any, any, any, any, any\>[] | [] |
`KeyArgs` | *Record*<string, [*Argument*](argument.md)<any, any, any, any, any, any, any\>\> | {} |

#### Parameters:

Name | Type | Description |
:------ | :------ | :------ |
`args` | [...Args[]] | List of argument schemas.   |
`keyargs` | KeyArgs | Keyed list of argument schemas.    |

**Returns:** [*Arguments*](arguments.md)<Args, KeyArgs\>

Defined in: src/Arguments.ts:194

## Accessors

### $

• get **$**(): *object*

Arguments list

**Returns:** *object*

Name | Type |
:------ | :------ |
`args` | [...Args[]] |
`keyargs` | KeyArgs |

Defined in: src/Arguments.ts:236

## Methods

### addArg

▸ **addArg**<Name, Arg\>(`arg`: Arg): [*Arguments*](arguments.md)<[...Args[], Arg], KeyArgs & { [K in string]: Arg}\>

Add an argument to the argument list.

#### Type parameters:

Name | Type |
:------ | :------ |
`Name` | *string* |
`Arg` | [*Argument*](argument.md)<Name, any, any, any, any, any, any, Arg\> |

#### Parameters:

Name | Type | Description |
:------ | :------ | :------ |
`arg` | Arg | Argument to add.    |

**Returns:** [*Arguments*](arguments.md)<[...Args[], Arg], KeyArgs & { [K in string]: Arg}\>

Defined in: src/Arguments.ts:229

___

### arg

▸ **arg**<Name, Arg\>(`name`: Name, `arg`: (`arg`: [*Argument*](argument.md)<Name, *null*, unknown, *false*, *false*, *null*, *null*\>) => Arg): [*Arguments*](arguments.md)<[...Args[], Arg], KeyArgs & { [K in string]: Arg}\>

Add an argument to the argument list via a callback.

The callback will provide the argument object to chain the setup.
The returned value of the callback will be appended to the arguments.

#### Type parameters:

Name | Type |
:------ | :------ |
`Name` | *string* |
`Arg` | [*Argument*](argument.md)<Name, any, any, any, any, any, any, Arg\> |

#### Parameters:

Name | Type | Description |
:------ | :------ | :------ |
`name` | Name | Name of argument.   |
`arg` | (`arg`: [*Argument*](argument.md)<Name, *null*, unknown, *false*, *false*, *null*, *null*\>) => Arg | Argument creation callback function.    |

**Returns:** [*Arguments*](arguments.md)<[...Args[], Arg], KeyArgs & { [K in string]: Arg}\>

Defined in: src/Arguments.ts:219
