[argvtype](../README.md) / [Exports](../modules.md) / Command

# Class: Command<Name, Desc, Flags, Help, Examples, Action, Args\>

Command

Holds the command schema.

A command is the a self contained executable function.

## Type parameters

Name | Type | Default |
:------ | :------ | :------ |
`Name` | *string* \| *typeof* ROOT | *typeof* ROOT |
`Desc` | *string* \| *null* | *null* |
`Flags` | *Record*<string, Flag<any, any, any, any, any, any, any, any\>\> | {} |
`Help` | *string* \| *null* | *null* |
`Examples` | { `example`: *string* ; `explain`: *string*  }[] | [] |
`Action` | (`config`: { `args`: { [K in keyof Args["$"]["keyargs"]]: Args["$"]["keyargs"][K] extends Argument<any, any, infer T, infer U, infer V, any, any\> ? U extends true ? V extends true ? T[] \| undefined : T : V extends true ? T \| undefined : T : never} ; `flags`: { [K in keyof Flags]: Flags[K] extends Flag<any, any, any, any, infer T, infer U, any, any, infer V\> ? U extends true ? V extends true ? T[] \| undefined : T[] : V extends true ? T \| undefined : T : never}  }) => *Promise*<void\> | () => *Promise*<void\> |
`Args` | [*Arguments*](arguments.md)<any\> | [*Arguments*](arguments.md)<[]\> |

## Table of contents

### Constructors

- [constructor](command.md#constructor)

### Accessors

- [$](command.md#$)

### Methods

- [action](command.md#action)
- [addArguments](command.md#addarguments)
- [addFlag](command.md#addflag)
- [arguments](command.md#arguments)
- [description](command.md#description)
- [example](command.md#example)
- [flag](command.md#flag)
- [help](command.md#help)
- [name](command.md#name)

## Constructors

### constructor

\+ **new Command**<Name, Desc, Flags, Help, Examples, Action, Args\>(): [*Command*](command.md)<Name, Desc, Flags, Help, Examples, Action, Args\>

Create new Root Command.

#### Type parameters:

Name | Type | Default |
:------ | :------ | :------ |
`Name` | *string* \| *typeof* ROOT | *typeof* ROOT |
`Desc` | *null* \| *string* | *null* |
`Flags` | *Record*<string, Flag<any, any, any, any, any, any, any, any, *false*\>\> | {} |
`Help` | *null* \| *string* | *null* |
`Examples` | { `example`: *string* ; `explain`: *string*  }[] | [] |
`Action` | (`config`: { `args`: { [K in string \| number \| symbol]: Args["$"]["keyargs"][K] extends Argument<any, any, T, U, V, any, any\> ? U extends true ? V extends true ? undefined \| T[] : T : V extends true ? undefined \| T : T : never} ; `flags`: { [K in string \| number \| symbol]: Flags[K] extends Flag<any, any, any, any, T, U, any, any, V\> ? U extends true ? V extends true ? undefined \| T[] : T[] : V extends true ? undefined \| T : T : never}  }) => *Promise*<void\> | () => *Promise*<void\> |
`Args` | [*Arguments*](arguments.md)<any, {}, Args\> | [*Arguments*](arguments.md)<[], {}\> |

**Returns:** [*Command*](command.md)<Name, Desc, Flags, Help, Examples, Action, Args\>

Defined in: src/Command.ts:49

\+ **new Command**<Name, Desc, Flags, Help, Examples, Action, Args\>(`name`: Name): [*Command*](command.md)<Name, Desc, Flags, Help, Examples, Action, Args\>

Create new Named Command.

#### Type parameters:

Name | Type | Default |
:------ | :------ | :------ |
`Name` | *string* \| *typeof* ROOT | *typeof* ROOT |
`Desc` | *null* \| *string* | *null* |
`Flags` | *Record*<string, Flag<any, any, any, any, any, any, any, any, *false*\>\> | {} |
`Help` | *null* \| *string* | *null* |
`Examples` | { `example`: *string* ; `explain`: *string*  }[] | [] |
`Action` | (`config`: { `args`: { [K in string \| number \| symbol]: Args["$"]["keyargs"][K] extends Argument<any, any, T, U, V, any, any\> ? U extends true ? V extends true ? undefined \| T[] : T : V extends true ? undefined \| T : T : never} ; `flags`: { [K in string \| number \| symbol]: Flags[K] extends Flag<any, any, any, any, T, U, any, any, V\> ? U extends true ? V extends true ? undefined \| T[] : T[] : V extends true ? undefined \| T : T : never}  }) => *Promise*<void\> | () => *Promise*<void\> |
`Args` | [*Arguments*](arguments.md)<any, {}, Args\> | [*Arguments*](arguments.md)<[], {}\> |

#### Parameters:

Name | Type | Description |
:------ | :------ | :------ |
`name` | Name | Name of command.    |

**Returns:** [*Command*](command.md)<Name, Desc, Flags, Help, Examples, Action, Args\>

Defined in: src/Command.ts:54

\+ **new Command**<Name, Desc, Flags, Help, Examples, Action, Args\>(`name`: Name, `description`: Desc, `flags`: Flags, `help`: Help, `examples`: Examples, `action`: Action, `args`: Args): [*Command*](command.md)<Name, Desc, Flags, Help, Examples, Action, Args\>

Create new Named Command.

#### Type parameters:

Name | Type | Default |
:------ | :------ | :------ |
`Name` | *string* \| *typeof* ROOT | *typeof* ROOT |
`Desc` | *null* \| *string* | *null* |
`Flags` | *Record*<string, Flag<any, any, any, any, any, any, any, any, *false*\>\> | {} |
`Help` | *null* \| *string* | *null* |
`Examples` | { `example`: *string* ; `explain`: *string*  }[] | [] |
`Action` | (`config`: { `args`: { [K in string \| number \| symbol]: Args["$"]["keyargs"][K] extends Argument<any, any, T, U, V, any, any\> ? U extends true ? V extends true ? undefined \| T[] : T : V extends true ? undefined \| T : T : never} ; `flags`: { [K in string \| number \| symbol]: Flags[K] extends Flag<any, any, any, any, T, U, any, any, V\> ? U extends true ? V extends true ? undefined \| T[] : T[] : V extends true ? undefined \| T : T : never}  }) => *Promise*<void\> | () => *Promise*<void\> |
`Args` | [*Arguments*](arguments.md)<any, {}, Args\> | [*Arguments*](arguments.md)<[], {}\> |

#### Parameters:

Name | Type | Description |
:------ | :------ | :------ |
`name` | Name | Name of command.   |
`description` | Desc | Short (1 line) description of the command.   |
`flags` | Flags | Flags of the command.   |
`help` | Help | Detailed help for the command.   |
`examples` | Examples | List of usage examples for the command.   |
`action` | Action | Action function the command will execute.   |
`args` | Args | List of arguments for the command.    |

**Returns:** [*Command*](command.md)<Name, Desc, Flags, Help, Examples, Action, Args\>

Defined in: src/Command.ts:62

## Accessors

### $

• get **$**(): *object*

Command configuration

**Returns:** *object*

Name | Type |
:------ | :------ |
`action` | Action |
`args` | Args |
`description` | Desc |
`examples` | Examples |
`flags` | Flags |
`help` | Help |
`name` | Name |

Defined in: src/Command.ts:251

## Methods

### action

▸ **action**<Action\>(`action`: Action): [*Command*](command.md)<Name, Desc, Flags, Help, Examples, Action, Args\>

Bind action callback to the command.

The action callback arguments are automatically inferred from the command object.

#### Type parameters:

Name | Type |
:------ | :------ |
`Action` | (`config`: { `args`: { [K in string \| number \| symbol]: Args["$"]["keyargs"][K] extends Argument<any, any, T, U, V, any, any\> ? U extends true ? V extends true ? undefined \| T[] : T : V extends true ? undefined \| T : T : never} ; `flags`: { [K in string \| number \| symbol]: Flags[K] extends Flag<any, any, any, any, T, U, any, any, V\> ? U extends true ? V extends true ? undefined \| T[] : T[] : V extends true ? undefined \| T : T : never}  }) => *Promise*<void\> |

#### Parameters:

Name | Type | Description |
:------ | :------ | :------ |
`action` | Action | Action to execute for the command.    |

**Returns:** [*Command*](command.md)<Name, Desc, Flags, Help, Examples, Action, Args\>

Defined in: src/Command.ts:231

___

### addArguments

▸ **addArguments**<Args\>(`args`: Args): [*Command*](command.md)<Name, Desc, Flags, Help, Examples, (`config`: { `args`: { [K in string \| number \| symbol]: Args["$"]["keyargs"][K] extends Argument<any, any, T, U, V, any, any\> ? U extends true ? V extends true ? undefined \| T[] : T : V extends true ? undefined \| T : T : never} ; `flags`: { [K in string \| number \| symbol]: Flags[K] extends Flag<any, any, any, any, T, U, any, any, V\> ? U extends true ? V extends true ? undefined \| T[] : T[] : V extends true ? undefined \| T : T : never}  }) => *Promise*<void\>, Args\>

Add arguments to the command.

#### Type parameters:

Name | Type |
:------ | :------ |
`Args` | [*Arguments*](arguments.md)<any, {}, Args\> |

#### Parameters:

Name | Type | Description |
:------ | :------ | :------ |
`args` | Args | Arguments to add.    |

**Returns:** [*Command*](command.md)<Name, Desc, Flags, Help, Examples, (`config`: { `args`: { [K in string \| number \| symbol]: Args["$"]["keyargs"][K] extends Argument<any, any, T, U, V, any, any\> ? U extends true ? V extends true ? undefined \| T[] : T : V extends true ? undefined \| T : T : never} ; `flags`: { [K in string \| number \| symbol]: Flags[K] extends Flag<any, any, any, any, T, U, any, any, V\> ? U extends true ? V extends true ? undefined \| T[] : T[] : V extends true ? undefined \| T : T : never}  }) => *Promise*<void\>, Args\>

Defined in: src/Command.ts:166

___

### addFlag

▸ **addFlag**<AddName, F\>(`flag`: F, `_name?`: AddName): [*Command*](command.md)<Name, Desc, Flags & { [i in string]: F}, Help, Examples, Action, Args\>

Add flag to the command.

#### Type parameters:

Name | Type |
:------ | :------ |
`AddName` | *string* |
`F` | *Flag*<AddName, any, any, any, any, any, any, any, any, F\> |

#### Parameters:

Name | Type |
:------ | :------ |
`flag` | F |
`_name?` | AddName |

**Returns:** [*Command*](command.md)<Name, Desc, Flags & { [i in string]: F}, Help, Examples, Action, Args\>

Defined in: src/Command.ts:220

___

### arguments

▸ **arguments**<AddArgs\>(`args`: (`args`: Args) => AddArgs): [*Command*](command.md)<Name, Desc, Flags, Help, Examples, (`config`: { `args`: { [K in string \| number \| symbol]: AddArgs["$"]["keyargs"][K] extends Argument<any, any, T, U, V, any, any\> ? U extends true ? V extends true ? undefined \| T[] : T : V extends true ? undefined \| T : T : never} ; `flags`: { [K in string \| number \| symbol]: Flags[K] extends Flag<any, any, any, any, T, U, any, any, V\> ? U extends true ? V extends true ? undefined \| T[] : T[] : V extends true ? undefined \| T : T : never}  }) => *Promise*<void\>, AddArgs\>

Add arguments to the command via a callback.

The callback will provide the arguments object to chain the setup.
The returned value of the callback will be set to the command.

#### Type parameters:

Name | Type |
:------ | :------ |
`AddArgs` | [*Arguments*](arguments.md)<any, {}, AddArgs\> |

#### Parameters:

Name | Type | Description |
:------ | :------ | :------ |
`args` | (`args`: Args) => AddArgs | Arguments creation callback function.    |

**Returns:** [*Command*](command.md)<Name, Desc, Flags, Help, Examples, (`config`: { `args`: { [K in string \| number \| symbol]: AddArgs["$"]["keyargs"][K] extends Argument<any, any, T, U, V, any, any\> ? U extends true ? V extends true ? undefined \| T[] : T : V extends true ? undefined \| T : T : never} ; `flags`: { [K in string \| number \| symbol]: Flags[K] extends Flag<any, any, any, any, T, U, any, any, V\> ? U extends true ? V extends true ? undefined \| T[] : T[] : V extends true ? undefined \| T : T : never}  }) => *Promise*<void\>, AddArgs\>

Defined in: src/Command.ts:134

___

### description

▸ **description**<Desc\>(`description`: Desc): [*Command*](command.md)<Name, Desc, Flags, Help, Examples, Action, Args\>

Set the description of the command.

#### Type parameters:

Name | Type |
:------ | :------ |
`Desc` | *null* \| *string* |

#### Parameters:

Name | Type | Description |
:------ | :------ | :------ |
`description` | Desc | Short (1 line) description of the command.    |

**Returns:** [*Command*](command.md)<Name, Desc, Flags, Help, Examples, Action, Args\>

Defined in: src/Command.ts:112

___

### example

▸ **example**<Explain, Example\>(`explain`: Explain, `example`: Example): [*Command*](command.md)<Name, Desc, Flags, Help, [...Examples[], { `example`: Example ; `explain`: Explain  }], Action, Args\>

Add an example for the command usage.

#### Type parameters:

Name | Type |
:------ | :------ |
`Explain` | *string* |
`Example` | *string* |

#### Parameters:

Name | Type | Description |
:------ | :------ | :------ |
`explain` | Explain | Explaination of what this command usage does.   |
`example` | Example | Command arguments and flags.    |

**Returns:** [*Command*](command.md)<Name, Desc, Flags, Help, [...Examples[], { `example`: Example ; `explain`: Explain  }], Action, Args\>

Defined in: src/Command.ts:122

___

### flag

▸ **flag**<Name, F\>(`name`: Name, `flag`: (`flag`: *Flag*<Name, *null*, *null*, *null*, boolean, *false*, *null*, *false*, *false*\>) => F): [*Command*](command.md)<Name, Desc, Flags & { [i in string]: F}, Help, Examples, Action, Args\>

Add a flag to the command via a callback.

The callback will provide the flag object to chain the setup.
The returned value of the callback will be added to the command.

#### Type parameters:

Name | Type |
:------ | :------ |
`Name` | *string* |
`F` | *Flag*<any, any, any, any, any, any, any, any, any, F\> |

#### Parameters:

Name | Type | Description |
:------ | :------ | :------ |
`name` | Name | Name of the flag.   |
`flag` | (`flag`: *Flag*<Name, *null*, *null*, *null*, boolean, *false*, *null*, *false*, *false*\>) => F | Flag creation callback function.    |

**Returns:** [*Command*](command.md)<Name, Desc, Flags & { [i in string]: F}, Help, Examples, Action, Args\>

Defined in: src/Command.ts:211

___

### help

▸ **help**<Help\>(`help`: Help): [*Command*](command.md)<Name, Desc, Flags, Help, Examples, Action, Args\>

Set the help of the command.

#### Type parameters:

Name | Type |
:------ | :------ |
`Help` | *null* \| *string* |

#### Parameters:

Name | Type | Description |
:------ | :------ | :------ |
`help` | Help | Detailed help for the command.    |

**Returns:** [*Command*](command.md)<Name, Desc, Flags, Help, Examples, Action, Args\>

Defined in: src/Command.ts:198

___

### name

▸ **name**<Name\>(`name`: Name): [*Command*](command.md)<Name, Desc, Flags, Help, Examples, Action, Args\>

Set the name of the command.

#### Type parameters:

Name | Type |
:------ | :------ |
`Name` | *string* |

#### Parameters:

Name | Type | Description |
:------ | :------ | :------ |
`name` | Name | Unique name of command.    |

**Returns:** [*Command*](command.md)<Name, Desc, Flags, Help, Examples, Action, Args\>

Defined in: src/Command.ts:103
