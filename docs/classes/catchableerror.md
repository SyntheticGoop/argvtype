[argvtype](../README.md) / [Exports](../modules.md) / CatchableError

# Class: CatchableError

CatchableError

The main error object which all errors in the app extends from.

If an error is thrown extending this class, it will be caught by the cli class and printed as a message without stack traces.

## Hierarchy

* *Error*

  ↳ **CatchableError**

## Table of contents

### Constructors

- [constructor](catchableerror.md#constructor)

### Properties

- [message](catchableerror.md#message)
- [name](catchableerror.md#name)
- [prepareStackTrace](catchableerror.md#preparestacktrace)
- [stack](catchableerror.md#stack)
- [stackTraceLimit](catchableerror.md#stacktracelimit)

### Methods

- [captureStackTrace](catchableerror.md#capturestacktrace)

## Constructors

### constructor

\+ **new CatchableError**(`message`: *string*): [*CatchableError*](catchableerror.md)

Create new CatchableError

#### Parameters:

Name | Type | Description |
:------ | :------ | :------ |
`message` | *string* | Message to print.    |

**Returns:** [*CatchableError*](catchableerror.md)

Overrides: void

Defined in: src/Error.ts:8

## Properties

### message

• **message**: *string*

Inherited from: void

Defined in: .yarn/cache/typescript-patch-99fe32b02b-a8956044aa.zip/node_modules/typescript/lib/lib.es5.d.ts:974

___

### name

• **name**: *string*

Inherited from: void

Defined in: .yarn/cache/typescript-patch-99fe32b02b-a8956044aa.zip/node_modules/typescript/lib/lib.es5.d.ts:973

___

### prepareStackTrace

• `Optional` **prepareStackTrace**: (`err`: Error, `stackTraces`: CallSite[]) => *any*

Optional override for formatting stack traces

**`see`** https://v8.dev/docs/stack-trace-api#customizing-stack-traces

#### Type declaration:

▸ (`err`: Error, `stackTraces`: CallSite[]): *any*

#### Parameters:

Name | Type |
:------ | :------ |
`err` | Error |
`stackTraces` | CallSite[] |

**Returns:** *any*

Defined in: .yarn/cache/@types-node-npm-14.14.35-c55176fef6-0f6320bf53.zip/node_modules/@types/node/globals.d.ts:11

Defined in: .yarn/cache/@types-node-npm-14.14.35-c55176fef6-0f6320bf53.zip/node_modules/@types/node/globals.d.ts:11

___

### stack

• `Optional` **stack**: *string*

Inherited from: void

Defined in: .yarn/cache/typescript-patch-99fe32b02b-a8956044aa.zip/node_modules/typescript/lib/lib.es5.d.ts:975

___

### stackTraceLimit

• **stackTraceLimit**: *number*

Defined in: .yarn/cache/@types-node-npm-14.14.35-c55176fef6-0f6320bf53.zip/node_modules/@types/node/globals.d.ts:13

## Methods

### captureStackTrace

▸ **captureStackTrace**(`targetObject`: *object*, `constructorOpt?`: Function): *void*

Create .stack property on a target object

#### Parameters:

Name | Type |
:------ | :------ |
`targetObject` | *object* |
`constructorOpt?` | Function |

**Returns:** *void*

Defined in: .yarn/cache/@types-node-npm-14.14.35-c55176fef6-0f6320bf53.zip/node_modules/@types/node/globals.d.ts:4
