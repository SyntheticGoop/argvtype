[argvtype](../README.md) / [Exports](../modules.md) / Group

# Class: Group<Name, Commands, Groups, Desc, Help\>

Group

Holds the group schema.

A Group is collection of recursive subgroups and subcommands.
Commands accessed within a group will be called with its parent group names recursively.

## Type parameters

Name | Type | Default |
:------ | :------ | :------ |
`Name` | *string* \| *typeof* ROOT | *typeof* ROOT |
`Commands` | *object* | {} |
`Groups` | *object* | {} |
`Desc` | *string* \| *null* | *null* |
`Help` | *string* \| *null* | *null* |

## Table of contents

### Constructors

- [constructor](group.md#constructor)

### Accessors

- [$](group.md#$)

### Methods

- [addCommand](group.md#addcommand)
- [addGroup](group.md#addgroup)
- [command](group.md#command)
- [description](group.md#description)
- [group](group.md#group)
- [help](group.md#help)
- [name](group.md#name)

## Constructors

### constructor

\+ **new Group**<Name, Commands, Groups, Desc, Help\>(): [*Group*](group.md)<Name, Commands, Groups, Desc, Help\>

Create new Root Group.

#### Type parameters:

Name | Type | Default |
:------ | :------ | :------ |
`Name` | *string* \| *typeof* ROOT | *typeof* ROOT |
`Commands` | *object* | {} |
`Groups` | *object* | {} |
`Desc` | *null* \| *string* | *null* |
`Help` | *null* \| *string* | *null* |

**Returns:** [*Group*](group.md)<Name, Commands, Groups, Desc, Help\>

Defined in: src/Group.ts:30

\+ **new Group**<Name, Commands, Groups, Desc, Help\>(`name`: Name): [*Group*](group.md)<Name, Commands, Groups, Desc, Help\>

Create new Named Group.

#### Type parameters:

Name | Type | Default |
:------ | :------ | :------ |
`Name` | *string* \| *typeof* ROOT | *typeof* ROOT |
`Commands` | *object* | {} |
`Groups` | *object* | {} |
`Desc` | *null* \| *string* | *null* |
`Help` | *null* \| *string* | *null* |

#### Parameters:

Name | Type | Description |
:------ | :------ | :------ |
`name` | Name | Unique name of group.    |

**Returns:** [*Group*](group.md)<Name, Commands, Groups, Desc, Help\>

Defined in: src/Group.ts:35

\+ **new Group**<Name, Commands, Groups, Desc, Help\>(`name`: Name, `commands`: Commands, `groups`: Groups, `desc`: Desc, `help`: Help): [*Group*](group.md)<Name, Commands, Groups, Desc, Help\>

Create new Named Group.

#### Type parameters:

Name | Type | Default |
:------ | :------ | :------ |
`Name` | *string* \| *typeof* ROOT | *typeof* ROOT |
`Commands` | *object* | {} |
`Groups` | *object* | {} |
`Desc` | *null* \| *string* | *null* |
`Help` | *null* \| *string* | *null* |

#### Parameters:

Name | Type | Description |
:------ | :------ | :------ |
`name` | Name | Unique name of group.   |
`commands` | Commands | Commands of the group.   |
`groups` | Groups | Sub-groups of the group.   |
`desc` | Desc | Short (1 line) description of the group.   |
`help` | Help | Detailed help for the group.    |

**Returns:** [*Group*](group.md)<Name, Commands, Groups, Desc, Help\>

Defined in: src/Group.ts:43

## Accessors

### $

• get **$**(): *object*

Command configuration

**Returns:** *object*

Name | Type |
:------ | :------ |
`commands` | Commands & Groups |
`description` | Desc |
`help` | Help |
`name` | Name |

Defined in: src/Group.ts:195

## Methods

### addCommand

▸ **addCommand**<AddCommand\>(`command`: AddCommand): [*Group*](group.md)<Name, *Omit*<Commands, AddCommand *extends* [*Command*](command.md)<T, any, any, any, any, any, any\> ? T : *never*\> & { [x in any]: AddCommand}, Groups, Desc, Help\>

Add a command to the group.

#### Type parameters:

Name | Type |
:------ | :------ |
`AddCommand` | [*Command*](command.md)<any, any, any, any, any, any, any, AddCommand\> |

#### Parameters:

Name | Type | Description |
:------ | :------ | :------ |
`command` | AddCommand | Command.    |

**Returns:** [*Group*](group.md)<Name, *Omit*<Commands, AddCommand *extends* [*Command*](command.md)<T, any, any, any, any, any, any\> ? T : *never*\> & { [x in any]: AddCommand}, Groups, Desc, Help\>

Defined in: src/Group.ts:153

___

### addGroup

▸ **addGroup**<AddName, AddGroup\>(`group`: AddGroup *extends* [*Group*](group.md)<any, T, U, V, W\> ? [*Group*](group.md)<AddName, T, U, V, W\> : *never*): [*Group*](group.md)<Name, Commands, *Omit*<Groups, AddName\> & { [x in string]: AddGroup}, Desc, Help\>

Add a sub-group to the group.

#### Type parameters:

Name | Type |
:------ | :------ |
`AddName` | *string* |
`AddGroup` | [*Group*](group.md)<AddName, any, any, any, any, AddGroup\> |

#### Parameters:

Name | Type | Description |
:------ | :------ | :------ |
`group` | AddGroup *extends* [*Group*](group.md)<any, T, U, V, W\> ? [*Group*](group.md)<AddName, T, U, V, W\> : *never* | Group    |

**Returns:** [*Group*](group.md)<Name, Commands, *Omit*<Groups, AddName\> & { [x in string]: AddGroup}, Desc, Help\>

Defined in: src/Group.ts:109

___

### command

▸ **command**<Name, AddCommand\>(`name`: Name, `command`: (`command`: [*Command*](command.md)<Name, *null*, {}, *null*, [], () => *Promise*<void\>, [*Arguments*](arguments.md)<[], {}\>\>) => AddCommand): [*Group*](group.md)<Name, *Omit*<Commands, Name\> & { [x in string]: AddCommand}, Groups, Desc, Help\>

Add a command to the group via a callback.

The callback will provide the command object to chain the setup.
The returned value of the callback will be added to the command.

#### Type parameters:

Name | Type |
:------ | :------ |
`Name` | *string* |
`AddCommand` | [*Command*](command.md)<any, any, any, any, any, any, any, AddCommand\> |

#### Parameters:

Name | Type | Description |
:------ | :------ | :------ |
`name` | Name | Name of the command.   |
`command` | (`command`: [*Command*](command.md)<Name, *null*, {}, *null*, [], () => *Promise*<void\>, [*Arguments*](arguments.md)<[], {}\>\>) => AddCommand | Command creation callback function.    |

**Returns:** [*Group*](group.md)<Name, *Omit*<Commands, Name\> & { [x in string]: AddCommand}, Groups, Desc, Help\>

Defined in: src/Group.ts:136

___

### description

▸ **description**<Desc\>(`desc`: Desc): [*Group*](group.md)<Name, Commands, Groups, Desc, Help\>

Set the description of the group.

#### Type parameters:

Name | Type |
:------ | :------ |
`Desc` | *null* \| *string* |

#### Parameters:

Name | Type |
:------ | :------ |
`desc` | Desc |

**Returns:** [*Group*](group.md)<Name, Commands, Groups, Desc, Help\>

Defined in: src/Group.ts:169

___

### group

▸ **group**<Name, AddGroup\>(`name`: Name, `group`: (`group`: [*Group*](group.md)<Name, {}, {}, *null*, *null*\>) => AddGroup): [*Group*](group.md)<Name, Commands, *Omit*<Groups, Name\> & { [x in string]: AddGroup}, Desc, Help\>

Add a sub-group to the group via a callback.

The callback will provide the group object to chain the setup.
The returned value of the callback will be added to the group.

#### Type parameters:

Name | Type |
:------ | :------ |
`Name` | *string* |
`AddGroup` | [*Group*](group.md)<any, any, any, any, any, AddGroup\> |

#### Parameters:

Name | Type | Description |
:------ | :------ | :------ |
`name` | Name | Name of the group.   |
`group` | (`group`: [*Group*](group.md)<Name, {}, {}, *null*, *null*\>) => AddGroup | Group creation callback function.    |

**Returns:** [*Group*](group.md)<Name, Commands, *Omit*<Groups, Name\> & { [x in string]: AddGroup}, Desc, Help\>

Defined in: src/Group.ts:92

___

### help

▸ **help**<Help\>(`help`: Help): [*Group*](group.md)<Name, Commands, Groups, Desc, Help\>

Set the help of the group.

#### Type parameters:

Name | Type |
:------ | :------ |
`Help` | *null* \| *string* |

#### Parameters:

Name | Type | Description |
:------ | :------ | :------ |
`help` | Help | Detailed help for the group.    |

**Returns:** [*Group*](group.md)<Name, Commands, Groups, Desc, Help\>

Defined in: src/Group.ts:184

___

### name

▸ **name**<Name\>(`name`: Name): [*Group*](group.md)<Name, Commands, Groups, Desc, Help\>

Set the name of the group.

#### Type parameters:

Name | Type |
:------ | :------ |
`Name` | *string* |

#### Parameters:

Name | Type | Description |
:------ | :------ | :------ |
`name` | Name | Unique name of group.    |

**Returns:** [*Group*](group.md)<Name, Commands, Groups, Desc, Help\>

Defined in: src/Group.ts:78
