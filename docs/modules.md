[argvtype](README.md) / Exports

# argvtype

## Table of contents

### Classes

- [Argument](classes/argument.md)
- [Arguments](classes/arguments.md)
- [CatchableError](classes/catchableerror.md)
- [Command](classes/command.md)
- [Group](classes/group.md)

### Functions

- [cli](modules.md#cli)
- [cmddir](modules.md#cmddir)

## Functions

### cli

▸ **cli**<CMD\>(`args`: *string*[], `cmd`: CMD): *Promise*<unknown\>

Parse the argument list and execute the action from the command schema.

if --help is one of the arguments, the interactive help menu will be opened.

#### Type parameters:

Name | Type |
:------ | :------ |
`CMD` | [*Group*](classes/group.md)<any, any, any, any, any\> \| [*Command*](classes/command.md)<any, any, any, any, any, any, any\> |

#### Parameters:

Name | Type | Description |
:------ | :------ | :------ |
`args` | *string*[] | Input argument list.   |
`cmd` | CMD | Command schema.    |

**Returns:** *Promise*<unknown\>

Defined in: src/cli.ts:22

▸ **cli**<CMD\>(`args`: *string*[], `cmd`: CMD, `helpflag`: *string*): *Promise*<unknown\>

Parse the argument list and execute the action from the command schema.

Set the helpflag value to override the help menu flag detection from the default of '--help' to something else.
The help option will be triggered whenever an argument contains this value exactly.

#### Type parameters:

Name | Type |
:------ | :------ |
`CMD` | [*Group*](classes/group.md)<any, any, any, any, any\> \| [*Command*](classes/command.md)<any, any, any, any, any, any, any\> |

#### Parameters:

Name | Type | Description |
:------ | :------ | :------ |
`args` | *string*[] | Input argument list.   |
`cmd` | CMD | Command schema.   |
`helpflag` | *string* | Help argument matching string.    |

**Returns:** *Promise*<unknown\>

Defined in: src/cli.ts:33

___

### cmddir

▸ **cmddir**(): [*Group*](classes/group.md)<any, any, any, any, any\>

Recursively searches and picks up commands from the calling directory.

In order to use directory discovery, each folder containing commands must have an index
file with a group linking all the subcommands (can be another dir function).
The index file must export the group as an ES6 default export, or as a CommonJS exports.default value.

**Returns:** [*Group*](classes/group.md)<any, any, any, any, any\>

Group object

Defined in: src/cmddir.ts:15

▸ **cmddir**(`root`: *string*): [*Group*](classes/group.md)<any, any, any, any, any\>

Recursively searches and picks up commands from a directory as subcommands.

In order to use directory discovery, each folder containing commands must have an index
file with a group linking all the subcommands (can be another dir function).
The index file must export the group as an ES6 default export, or as a CommonJS exports.default value.

#### Parameters:

Name | Type | Description |
:------ | :------ | :------ |
`root` | *string* | Root folder to begin searching.   |

**Returns:** [*Group*](classes/group.md)<any, any, any, any, any\>

Group object

Defined in: src/cmddir.ts:26

▸ **cmddir**<G\>(`root`: *string*, `group`: G): G

Recursively searches and picks up commands from a directory as subcommands, with the group object as the root command.

In order to use directory discovery, each folder containing commands must have an index
file with a group linking all the subcommands (can be another dir function).
The index file must export the group as an ES6 default export, or as a CommonJS exports.default value.

#### Type parameters:

Name | Type |
:------ | :------ |
`G` | [*Group*](classes/group.md)<any, any, any, any, any, G\> |

#### Parameters:

Name | Type | Description |
:------ | :------ | :------ |
`root` | *string* | Root folder to begin searching.   |
`group` | G | Group object to bind to directory command.   |

**Returns:** G

Group object

Defined in: src/cmddir.ts:38
