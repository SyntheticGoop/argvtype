argvtype / [Exports](modules.md)

# argvtype

`Argvtype` (pronounced are-vee-type) is a cli framework for building typed CLIs.

`Argvtype` is designed with 4 goals in mind:

1. A proper intellisense types with typescript for arguments and flags.
2. Better, more interactive help.
3. Composable and straightforward to use.
4. Good support for sub commands.

## Installation

Install with your package manager of choice.

```bash
yarn add argvtype
```

## Example Usage

Let's see what an example of `argvtype` in use could look like.

```ts
import { cli, Group, CatchableError } from 'argvtype'

class FiniteNumber extends CatchableError {
  constructor(value: any) {
    super(`Parsed value: ${value} does not cast into a valid number!`)
  }
}

const cmd = new Group()
  .description('Order some food from a menu.')
  .help(`With this command you can pick from multiple menus and decide on which foods to order.
The available menus are: Breakfast, Lunch, and Dinner.`)

  .group('breakfast', group => group
    .description('The breakfast menu.')
    .help(`The breakfast menu is for early risers.
For breakfast, you choose the eggs and sausages you want.`)

    .command('sausage', cmd => cmd
      .description('Sausages in various styles and flavours.')
      .help(`Sausages are a common source of protein in breakfasts.
Choosing the right sausage could very well make your day!`)
      .arguments(args => args
        .arg('type', arg => arg
          .description('Style of sausage')
          .enum('Bratwurst', 'Lap cheong', 'Vienna')
          .type('style', x => x)
        )
        .arg('quantity', arg => arg
          .description('Quantity of sausages')
          .optional()
          .type('number', x => {
            if (!x) throw new FiniteNumber(x)
            const parsed = +x
            if (!Number.isFinite(parsed)) throw new FiniteNumber(x)
            return parsed
          })
          .default(3)
        )
      )
      .flag('doneness', flag => flag
        .description('The doneness of the sausage')
        .letter('d')
        .enum('raw', 'medium-rare', 'medium-well', 'well-done')
        .optional()
      )
    )

    .command('eggs', cmd => cmd
      .description('Egg selection')
      .help(`Do you like your eggs sunny side up, scrambled or half boiled?`)
      .arguments(args => args
        .arg('type', arg => arg
          .description('Style of egg')
          .enum('sunny', 'scrambled', 'half')
          .type('style', x => x)
        )
        .arg('quantity', arg => arg
          .description('Quantity of sausages')
          .optional()
          .type('number', x => {
            if (!x) throw new FiniteNumber(x)
            const parsed = +x
            if (!Number.isFinite(parsed)) throw new FiniteNumber(x)
            return parsed
          })
          .default(3)
        )
      )
    )
  )

  .group('lunch', group => group
    .description('The lunch menu.')
    .help('The standard lunch menu.\nPick from a standard set of meals.')

    .command('porridge', cmd => cmd
      .description('Porridge')
      .help(`Poor man's porridge.`)
      .arguments(args => args
        .arg('type', arg => arg
          .description('Type of porridge')
          .enum('fish', 'chicken', 'pork')
          .required()
        )
      )
    )
    
    .command('chicken-rice', cmd => cmd
      .description('Chicken rice')
      .help('Standard chicken rice. Hinanese style.')
      .flag('chilli', flag => flag.optional().default(false))
      .flag('ketchup', flag => flag.optional().default(false))
      .flag('white', flag => flag.optional().default(false))
      .example('Get chicken rice', '')
     )
  )

  .group('dinner', group => group
    .description('The dinner menu.')
    .help('The standard dinner menu.\nPick from a standard set of meals or burgers.')

    .command('steak', cmd => cmd
      .description('Great steaks')
      .help(`Choose from a variety of steaks`)
      .arguments(args => args
        .arg('cuts', arg => arg
          .description('Steak cuts')
          .enum('rib-eye', 'sirloin', 't-bone')
          .required()
        )
      )
      .flag('doneness', flag => flag
        .description('The doneness of the sausage')
        .letter('d')
        .enum('raw', 'medium-rare', 'medium-well', 'well-done')
        .optional()
      )
    )

    .group('burger', group => group
      .description('Many great burgers.')
      .help('An assortment of burgers.')
      
      .command('quarter-pounder', cmd => cmd
        .description('For the meat lovers.')
        .help('A heavy duty solution.')
      )

      .command('cheeseburger', cmd => cmd
        .description('American cheesebruger.')
        .help('For the cheese lovers.')
      )

      .command('quarter-pounder', cmd => cmd
        .description('Your daily intake of fish')
        .help('Not very healthy.')
      )
    )
  )

cli(process.argv.slice(2), cmd)
```

My word! What is that wall of code trying to do?

The cli declaration above declares a command line application with the following subcommands:

```text
app
 +- breakfast
     +- eggs
     +- sausage
 +- lunch
     +- chicken-rice
     +- porridge
 +- dinner
     +- steak
     +- burger
         +- cheeseburger
         +- fishburger
         +- quarter-pounder
```

It is composed of the following objects:

- Each parent object is built using the `Group` class.
- Each terminating command is built using the `Command` class.
- Each command flag is built using the `Flag` class.
- Each argument is built using the `Argument` class, which in turn is composed with the `Arguments` class.

The classes are composed like this:

```text
Group
 +- Command
     +- Flag
     +- Arguments
         +- Argument
```

Every parent class will provide a constructor function for its child component, providing a callback for it.

This allows you construct the entire application by just starting with the `Group` class.

It is still highly reccomended to split up your application into smaller chunks.

After constructing your cli, run it using the `cli` function

```ts
import { cli } from 'argvparse'

cli(process.argv.slice(2), <your command or group object>)
```

You can access this example in the `examples` directory on the repo. You can also run the commands directly if you clone
this repository and run `yarn example:food-menu` or `yarn example:food-menu-dir`

## Interactive Help

So far we have seen what it's like to construct a CLI application. You may have noticed that the `Group` and `Command` objects
have the `.help()` and `.description()` methods on them, while the `Flag` and `Argument` objects have `.description()`.

These methods are used to bind help menus to the objects. When the application is run and one of the arguments is `--help`,
an interactive help menu will open, allowing you to transverse the menu and search for the command you need.

When building the CLI, you will probably want to ensure that the help menu is never too long that it overflows the maximum
height of the current terminal.

The help flag `--help` is declared globally. You cannot reuse this flag inside your commands. If you need to use `--help` for
something else, you can override `--help` with the third argument of then `cli` function.

Override `--help` with `-h`

```ts
import { cli } from 'argvparse'

cli(process.argv.slice(2), <your command or group object>, '-h')
```

## Testing

In order to test the application, you can import the `MockRender` class from `argvparse/build/cjs/test` or `argvparse/build/esm/test` and inject it into the cli function.

```ts
import { MockRenderer } from 'argvparse/build/cjs/test'
import { cli } from 'argvparse'

const logs = []
const log = (...log: any[]) => logs.push(log)

const renderer = new MockRenderer()

const mocked_cli = cli.bind({ log, renderer: renderer.render as any, noexit: true })

mocked_cli(['test', 'args'], <your command or group object>)
```

## Directory Discovery

Instead of building the application by nesting commands, you can use directory discovery.

Each directory must have an `index` file with a `Group` linking all the subcommands (can be another dir function) or `Command`.
The index file must export the group as an `ES6` `default export`, or as a `CommonJS` `exports.default` value.

You can recursively use the `cmddir` function.

### Command

```ts
import { Command } from 'argvparse'

export default new Command()
// or
exports.default = new Command()
```

### Main directory

```ts
import { cmddir, cli } from 'argvparse'

const detect = cmddir(__dirname)

cli(process.argv.slice(2), detect)
```

## API

Refer to [typedoc](docs/modules.md)
