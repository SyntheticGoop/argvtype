import { UNSET } from './singletons/UNSET'

export type AllowedLetters =
  'a' | 'A' | 'b' | 'B' | 'c' | 'C' | 'd' | 'D' | 'e' | 'E' | 'f' | 'F' |
  'g' | 'G' | 'h' | 'H' | 'i' | 'I' | 'j' | 'J' | 'k' | 'K' | 'l' | 'L' |
  'm' | 'M' | 'n' | 'N' | 'o' | 'O' | 'p' | 'P' | 'q' | 'Q' | 'r' | 'R' |
  's' | 'S' | 't' | 'T' | 'u' | 'U' | 'v' | 'V' | 'w' | 'W' | 'x' | 'X' |
  'y' | 'Y' | 'z' | 'Z'

/**
 * Flag
 * 
 * Holds the Flag schema.
 * 
 * A flag is a keyed argument which can be passed to the command line.
 */
export class Flag<
  Name extends string,
  Letter extends AllowedLetters | null = null,
  Desc extends string | null = null,
  TypeName extends string | null = null,
  Type extends unknown = boolean,
  Multiple extends boolean = false,
  Restrict extends string[] | null = null,
  Invert extends boolean = false,
  Optional extends boolean = false,
  > {


  /**
   * Create new Flag
   * 
   * @param name Long name of flag.
   */
  constructor(
    name: Name,
  )
  /**
   * Create new Flag
   * 
   * @param name Long name of flag.
   * @param description Short (1 line) description of flag.
   * @param typename Set the type name of the flag variable.
   * @param cast Function to cast input value to output type. Throws class extending CatchableError on calst failure.
   * @param flag Letter of flag.
   * @param variadic Set to true to make the flag variadic.
   * @param enums Restrictive list of strings that the flag can accept.
   * @param invert Reverse the flag if it is a boolean.
   * @param defaults The default value of the flag.
   */
  constructor(
    name: Name,
    description: Desc,
    typename: TypeName,
    cast: ((input: Restrict extends Array<infer T> ? T : never) => Type) | null,
    flag: Letter,
    variadic: Multiple,
    enums: Restrict,
    invert: Invert,
    optional: Optional,
    defaults: (Multiple extends true ? Type[] : Type) | typeof UNSET
  )
  constructor(
    private _name: Name,
    private _description: Desc = null as Desc,
    private _typename: TypeName = null as TypeName,
    private _cast: ((input: Restrict extends Array<infer T> ? T : never) => Type) | null = null as null,
    private _flag: Letter = null as Letter,
    private _variadic: Multiple = false as Multiple,
    private _enum: Restrict = null as Restrict,
    private _invert: Invert = false as Invert,
    private _optional: Optional = false as Optional,
    private _default: (Multiple extends true ? Type[] : Type) | typeof UNSET = UNSET,
  ) { }

  /**
   * Set the letter of the flag.
   * 
   * @param flag Letter of flag.
   */
  public letter<Letter extends AllowedLetters | null>(letter: Letter) {
    return new Flag(this._name, this._description, this._typename, this._cast, letter, this._variadic, this._enum, this._invert, this._optional, this._default)
  }

  /**
   * Set the description of the flag
   * 
   * @param description Short (1 line) description of flag.
   */
  public description<Desc extends string | null>(description: Desc) {
    return new Flag(this._name, description, this._typename, this._cast, this._flag, this._variadic, this._enum, this._invert, this._optional, this._default)
  }

  /**
   * Set the type, type name and casting function of the flag.
   * 
   * @param name Set the type name of the flag.
   * @param cast Function to cast the input value ot the output type. Throws class extending CatchableError on cast failure.
   */
  public type<TypeName extends string | null, Type>(name: TypeName, cast: (input: Restrict extends Array<infer T> ? T : string) => Type = x => x as unknown as Type) {
    return new Flag(this._name, this._description, name, cast, this._flag, this._variadic, this._enum, this._invert, this._optional, this._default as Multiple extends true ? Type[] : Type)
  }

  /**
   * Set the flag to be inverted.
   */
  public invert(): Flag<Name, Letter, Desc, TypeName, Type, Multiple, Restrict, true, Optional>
  /**
   * Set the flag to be inverted.
   * 
   * @param invert Set to true to make flag inverted.
   */
  public invert<Invert extends boolean>(invert: Invert): Flag<Name, Letter, Desc, TypeName, Type, Multiple, Restrict, Invert, Optional>
  public invert<Invert extends boolean>(invert: Invert = true as Invert): Flag<Name, Letter, Desc, TypeName, Type, Multiple, Restrict, Invert, Optional> {
    return new Flag(this._name, this._description, this._typename, this._cast, this._flag, this._variadic, this._enum, invert, this._optional, this._default)
  }


  /**
   * Set the type and casting function of the flag.
   * 
   * @param cast Function to cast the input value to the output type. Throws class extending CatchableError on cast failure.
   */
  public coerce<Type>(cast: (input: Restrict extends Array<infer T> ? T : never) => Type) {
    return new Flag(this._name, this._description, this._typename, cast, this._flag, this._variadic, this._enum, this._invert, this._optional, this._default as Multiple extends true ? Type[] : Type)
  }


  /**
   * Set argument to be variadic.
   */
  public variadic(): Flag<Name, Letter, Desc, TypeName, Type, true, Restrict, Invert, Optional>
  /**
   * Set argument to be variadic.
   * 
   * @param variadic Set to true to make argument variadic.
   */
  public variadic<Multiple extends boolean>(multiple: Multiple): Flag<Name, Letter, Desc, TypeName, Type, Multiple, Restrict, Invert, Optional>
  public variadic<Multiple extends boolean>(multiple: Multiple = true as Multiple): Flag<Name, Letter, Desc, TypeName, Type, Multiple, Restrict, Invert, Optional> {
    return new Flag(this._name, this._description, this._typename, this._cast, this._flag, multiple, this._enum, this._invert, this._optional, this._default === UNSET ? UNSET : multiple ? (this._variadic ? this._default : [this._default]) : (this._variadic ? (this._default as any[])[0] : this._default))
  }


  /**
   * Set the allowed values of the flag.
   * 
   * @param restrict Restrictive list of strings that the flag can accept.
   */
  public enum<Restrict extends string[]>(...restrict: [...Restrict]) {
    const typename = this._typename
    type ExtendType = typeof typename extends null ? Restrict extends Array<infer T> ? T : never : Type
    const def = this._default
    type DefaultType = (typeof def) extends typeof UNSET ? typeof UNSET : ExtendType
    return new Flag(this._name, this._description, this._typename ?? 'enum', this._cast as (x: Restrict extends Array<infer T> ? T : never) => ExtendType, this._flag, this._variadic, restrict, this._invert, this._optional, this._default as DefaultType as any)
  }

  /**
   * Set the default value of the flag.
   * 
   * @param defaults The default value of the flag.
   */
  public default<T extends (unknown extends Type ? (Multiple extends true ? any[] : any) : (Multiple extends true ? Type[] : Type))>(defaults: T) {
    return new Flag(this._name, this._description, this._typename, this._cast, this._flag, this._variadic, this._enum, this._invert, this._optional, defaults)
  }

  /**
    * Set flag to be required.
    */
  public required(): Flag<Name, Letter, Desc, TypeName, Type, Multiple, Restrict, Invert, false>
  public required<Optional extends boolean>(optional: Optional = false as Optional): Flag<Name, Letter, Desc, TypeName, Type, Multiple, Restrict, Invert, Optional> {
    return new Flag(this._name, this._description, this._typename, this._cast, this._flag, this._variadic, this._enum, this._invert, optional, this._default)
  }

  /**
   * Set flag to be optional.
   */
  public optional(): Flag<Name, Letter, Desc, TypeName, Type, Multiple, Restrict, Invert, true>
  /**
    * Set flag optional state.
    * 
    * @param optional Set to true to make flag optional.
    */
  public optional<Optional extends boolean>(optional: Optional): Flag<Name, Letter, Desc, TypeName, Type, Multiple, Restrict, Invert, Optional>
  public optional<Optional extends boolean>(optional: Optional = true as Optional): Flag<Name, Letter, Desc, TypeName, Type, Multiple, Restrict, Invert, Optional> {
    return new Flag(this._name, this._description, this._typename, this._cast, this._flag, this._variadic, this._enum, this._invert, optional, this._default)
  }

  /** Flag Configuration */
  get $() {
    return {
      name: this._name,
      flag: this._flag,
      description: this._description,
      boolean: this._typename ? false : true,
      typed: this._typename ? {
        name: this._typename,
        cast: this._cast,
        variadic: this._variadic,
        enum: this._enum,
      } : null,
      invert: this._invert,
      default: this._default
    }
  }
}
