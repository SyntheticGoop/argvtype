import { Command } from './Command'
import { CatchableError } from './Error'
import { ROOT } from './singletons/ROOT'

export class GroupSubgroupNameMissing extends CatchableError {
  constructor() {
    super('Child group requires name')
  }
}
export class GroupSubcommandNameMissing extends CatchableError {
  constructor() {
    super('Child group requires name')
  }
}

/**
 * Group
 * 
 * Holds the group schema.
 * 
 * A Group is collection of recursive subgroups and subcommands.
 * Commands accessed within a group will be called with its parent group names recursively.
 */
export class Group<
  Name extends string | typeof ROOT = typeof ROOT,
  Commands extends { [i: string]: Command<any, any, any, any> } = {},
  Groups extends { [i: string]: Group<any, any, any, any, any> } = {},
  Desc extends string | null = null,
  Help extends string | null = null,
  > {

  /**
   * Create new Root Group.
   */
  constructor()
  /**
   * Create new Named Group.
   * 
   * @param name Unique name of group.
   */
  constructor(
    name: Name,
  )
  /**
   * Create new Named Group.
   * 
   * @param name Unique name of group.
   * @param commands Commands of the group.
   * @param groups Sub-groups of the group.
   * @param desc Short (1 line) description of the group.
   * @param help Detailed help for the group.
   */
  constructor(
    name: Name,
    commands: Commands,
    groups: Groups,
    desc: Desc,
    help: Help
  )
  constructor(
    private _name: Name = ROOT as Name,
    private _commands: Commands = {} as Commands,
    private _groups: Groups = {} as Groups,
    private _desc: Desc = null as Desc,
    private _help: Help = null as Help
  ) {
    this.group = this.group.bind(this)
    this.addGroup = this.addGroup.bind(this)
    this.command = this.command.bind(this)
    this.addCommand = this.addCommand.bind(this)
  }

  /**
   * Set the name of the group.
   * 
   * @param name Unique name of group.
   */
  public name<Name extends string>(name: Name) {
    return new Group(name, this._commands, this._groups, this._desc, this._help)
  }


  /**
   * Add a sub-group to the group via a callback.
   * 
   * The callback will provide the group object to chain the setup.
   * The returned value of the callback will be added to the group.
   * 
   * @param name Name of the group.
   * @param group Group creation callback function.
   */
  public group<Name extends string, AddGroup extends Group<any, any, any, any, any>>(name: Name, group: (group: Group<Name>) => AddGroup) {
    const c = group(new Group(name))
    return new Group(
      this._name,
      this._commands,
      { ...this._groups, [name]: c } as unknown as Omit<Groups, Name> & { [x in Name]: AddGroup },
      this._desc,
      this._help
    )
  }


  /**
   * Add a sub-group to the group.
   * 
   * @param group Group
   */
  public addGroup<AddName extends string, AddGroup extends Group<AddName, any, any, any, any>>(group: AddGroup extends Group<any, infer T, infer U, infer V, infer W> ? Group<AddName, T, U, V, W> : never): Group<
    Name,
    Commands,
    Omit<Groups, AddName> & { [x in AddName]: AddGroup },
    Desc,
    Help
  > {
    if (group._name as any === ROOT) throw new GroupSubgroupNameMissing()
    return new Group(
      this._name,
      this._commands,
      { ...this._groups, [group._name]: group } as unknown as Omit<Groups, AddName> & { [x in AddName]: AddGroup },
      this._desc,
      this._help
    )
  }


  /**
   * Add a command to the group via a callback.
   * 
   * The callback will provide the command object to chain the setup.
   * The returned value of the callback will be added to the command.
   * 
   * @param name Name of the command.
   * @param command Command creation callback function.
   */
  public command<Name extends string, AddCommand extends Command<any, any, any, any, any, any, any>>(name: Name, command: (command: Command<Name>) => AddCommand) {
    const c = command(new Command(name))
    return new Group(
      this._name,
      { ...this._commands, [name]: c } as Omit<Commands, Name> & { [x in Name]: AddCommand },
      this._groups,
      this._desc,
      this._help
    )
  }


  /**
   * Add a command to the group.
   * 
   * @param command Command.
   */
  public addCommand<AddCommand extends Command<any, any, any, any, any, any, any>>(command: AddCommand) {
    if (command.$.name === ROOT) throw new GroupSubcommandNameMissing()
    return new Group(
      this._name,
      { ...this._commands, [command.$.name]: command } as Omit<Commands, AddCommand extends Command<infer T, any, any, any, any, any, any> ? T : never> & { [x in AddCommand extends Command<infer T, any, any, any, any, any, any> ? T : never]: AddCommand },
      this._groups,
      this._desc,
      this._help
    )
  }

  /**
   * Set the description of the group.
   * 
   * @param description Short (1 line) description of the group.
   */
  public description<Desc extends string | null>(desc: Desc) {
    return new Group(
      this._name,
      this._commands,
      this._groups,
      desc,
      this._help
    )
  }

  /**
   * Set the help of the group.
   * 
   * @param help Detailed help for the group.
   */
  public help<Help extends string | null>(help: Help) {
    return new Group(
      this._name,
      this._commands,
      this._groups,
      this._desc,
      help
    )
  }

  /** Command configuration */
  get $() {
    return {
      name: this._name,
      help: this._help,
      description: this._desc,
      commands: {
        ...this._commands,
        ...this._groups,
      }
    }
  }
}