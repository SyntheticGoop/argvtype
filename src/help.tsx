import { Box, render, Text, useApp, useFocus, useInput } from 'ink'
import React, { Fragment, useEffect, useState } from 'react'
import { Group } from './Group'
import { ROOT } from './singletons/ROOT'
import { UNSET } from './singletons/UNSET'

type HelpArg = {
  name: string
  description: string | null
  enum: string[] | null
  typename: string | null
  optional: boolean
  variadic: boolean
  default: any
}

type HelpFlag = {
  name: string
  flag: string | null
  description: string | null
  typed: {
    name: string
    enum: string[] | null
    variadic: boolean
  } | null
  invert: boolean
  default: any
}



type HelpCommand = {
  name: string
  description: string | null
  help: string | null
  examples: Array<{ example: string, explain: string }>
  args: { $: { args: Array<{ $: HelpArg }> } }
  flags: Record<string, { $: HelpFlag }>
}

type HelpApp = {
  name: string | typeof ROOT
  description: string | null
  help: string | null
  commands: Record<string, { $: HelpCommand | HelpApp }>
}


const Command = (command: HelpCommand) => {
  function mapargs({ $: args }: { $: HelpArg }, i: number) {
    return <Text color={args.optional ? 'yellowBright' : 'greenBright'} key={i}>
      {i === 0 ? '' : ' '}
      {args.optional ? '<' : '['}
      {args.name}
      <Text color='gray'>{args.variadic ? '...' : ''}</Text>
      {args.optional ? '>' : ']'}
    </Text>
  }

  const arglen = command.args.$.args.reduce((a, c) => a < c.$.name.length ? c.$.name.length : a, 0)
  const argtypelen = command.args.$.args.map(x => x.$.enum ? x.$.enum.join(', ') : x.$.typename ? `[${x.$.typename}${x.$.variadic ? '...' : ''}]` : '').reduce((a, c) => a < c.length ? c.length : a, 0)
  const flaglen = Object.values(command.flags).map(x => (
    x.$.flag ?
      `-${x.$.flag}, ` :
      '    '
  ) + `--${x.$.name}`
  ).reduce((a, c) => a < c.length ? c.length : a, 0)
  const flagtypelen = Object.values(command.flags).map(x =>
    x.$.typed ?
      x.$.typed.variadic ?
        x.$.typed.name + '...' :
        x.$.typed.name :
      ''
  ).reduce((a, c) => a < c.length ? c.length : a, 0)
  const flagdesclen = Object.values(command.flags).map(x => x.$.typed?.enum ? x.$.typed.enum.join(', ') : x.$.typed?.name ? `[${x.$.typed.name}]` : '').reduce((a, c) => a < c.length ? c.length : a, 0)

  return (
    <Box flexDirection="column" marginTop={1} marginX={2}>
      <Text>Usage: <Text color='green'>{command.name}{Object.keys(command.flags).length > 0 ? <Text color='cyanBright'> {"{--flags}"}</Text> : <></>}</Text> {...command.args.$.args.map(mapargs)}</Text>
      {!command.description ? <Fragment></Fragment> : <Fragment>
        <Box height={1}></Box>
        <Text>{command.description}</Text>
      </Fragment>
      }
      {!command.help ? <Fragment></Fragment> : <Fragment>
        <Box height={1}></Box>
        <Text>{command.help}</Text>
      </Fragment>
      }
      {command.args.$.args.length === 0 ? <Fragment></Fragment> : <Fragment>
        <Box height={2}></Box>
        <Text underline={true}>Arguments</Text>
        <Box marginLeft={2} marginTop={1} flexDirection="column">
          {command.args.$.args.map(({ $: x }, i) => (
            <Box key={i} marginTop={i === 0 ? 0 : 1}>
              <Box flexShrink={0} width={arglen} marginRight={4}>
                <Text color={x.optional ? 'yellowBright' : 'greenBright'}>{x.name}</Text>
              </Box>
              <Box flexShrink={0} width={argtypelen} marginRight={4}>
                <Text>
                  {x.enum ? x.enum.join(', ') : x.typename ? `[${x.typename}${x.variadic ? '...' : ''}]` : ''}
                </Text>
              </Box>
              <Box width={(process.stdout.columns ?? 80) - (arglen + argtypelen + 8 + 4 + 2)}>
                <Text>{x.description}{x.default !== UNSET ? ` (default: ${JSON.stringify(x.default)})` : ``}</Text>
              </Box>
            </Box>
          ))}
        </Box>
      </Fragment>}
      {Object.keys(command.flags).length === 0 ? <Fragment></Fragment> : <Box marginTop={2} display="flex" flexDirection='column'>
        <Text underline={true}>Options</Text>
        <Box marginLeft={2} marginTop={1} flexDirection="column">
          {Object.values(command.flags).map((x, i) => (
            <Box key={i} marginTop={i === 0 ? 0 : 1}>
              <Box flexShrink={0} width={flaglen - (Object.values(command.flags).find(x => x.$.flag) ? 0 : 4)} marginRight={4}>
                <Text color="cyanBright">{(x.$.flag ? `-${x.$.flag}, ` : Object.values(command.flags).find(x => x.$.flag) ? '    ' : '') + `--${x.$.name}`}</Text>
              </Box>
              <Box flexShrink={0} width={flagtypelen} marginRight={4}>
                <Text>
                  {
                    x.$.typed ?
                      (x.$.typed.variadic ?
                        `${x.$.typed.name}...` :
                        x.$.typed.name) :
                      ''
                  }
                </Text>
              </Box>
              <Box flexShrink={0} width={flagdesclen} marginRight={4}>
                <Text>
                  {x.$.typed?.enum ? x.$.typed.enum.join(', ') : x.$.typed?.name ? `[${x.$.typed?.name}]` : ''}
                </Text>
              </Box>
              <Box>
                <Text>{x.$.description}{x.$.default !== UNSET ? ` (default: ${JSON.stringify(x.$.default)})` : ``}</Text>
              </Box>
            </Box>
          ))}
        </Box>
      </Box>}
      {command.examples.length === 0 ? <Fragment></Fragment> : <Fragment>
        <Box marginTop={1}></Box>
        <Text underline={true}>Examples</Text>
        <Box marginLeft={2} marginTop={-1} flexDirection="column">
          {command.examples.map((x, i) => (
            <Box key={i} marginTop={2} flexDirection="column">
              <Text>{command.name} {x.example}</Text>
              <Text color='gray'>{x.explain}</Text>
            </Box>
          ))}
        </Box>
      </Fragment>}
    </Box>
  )
}

const Section = ({ section, active, focused, key_length, nocursor }: { section: HelpCommand | HelpApp, active: () => void, focused: string, key_length: number, nocursor: boolean }) => {
  const { isFocused } = useFocus({ autoFocus: true })
  if (isFocused) active()
  return (
    <Box>
      <Box marginRight={2} marginLeft={-2} flexShrink={0} width={key_length + 2}>
        <Text>{(focused === section.name && !nocursor ? '> ' : '  ')}</Text><Text color='green'>{(section.name === ROOT ? '' : section.name)}</Text>
      </Box>
      <Box>
        <Text>{section.description}</Text>
      </Box>
    </Box>
  )
}
const Help = ({ app, jump, first_paint, nocursor }: { app: HelpApp | HelpCommand, jump: string[], first_paint: () => void, nocursor: boolean }) => {
  const [stack, setStack] = useState<Array<HelpApp | HelpCommand>>([app])
  function current(s = stack) {
    return s[s.length - 1]
  }
  let [active, setActive] = useState((() => {
    const c = current()
    if (c && 'commands' in c) {
      return Object.keys(c.commands)[0] ?? ''
    }
    return ''
  })())

  useEffect(() => {
    if (jump.length > 0) {
      const new_stack = stack.concat()
      for (const j of jump) {
        const last = current(new_stack)
        if (!last || !('commands' in last)) break
        const next = last.commands[j]
        if (next) new_stack.push(next.$)
      }
      setStack(new_stack)
      first_paint()
    }
  }, [])

  useInput((_input, key) => {
    const c = current()
    if (c && 'commands' in c && key.upArrow) {
      const keys = Object.keys(c.commands)
      const i = keys.findIndex(x => x === active)
      setActive(i <= 0 ? keys[0] : keys[i - 1])
    }
    if (c && 'commands' in c && key.downArrow) {
      const keys = Object.keys(c.commands)
      const i = keys.findIndex(x => x === active)
      setActive(i === -1 ? keys[0] : (i + 1 === keys.length) ? keys[i] : keys[i + 1])
    }
    if (c && 'commands' in c && (key.rightArrow || key.return)) {
      const next = c.commands[active]
      if (!next) return
      setStack([...stack, next.$])
      if (next instanceof Group) {
        setActive(Object.keys(next.$.commands)[0])
      }
    }
    if (key.leftArrow || key.backspace) {
      if (stack.length === 1) {
        exit()
      } else {
        if (c.name !== ROOT) setActive(c.name)
        setStack(stack.slice(0, -1))
      }
    }
    if (key.escape) {
      exit()
    }
  })

  function next_explain() {
    const c = current()
    if (!c) return
    if ('commands' in c) {
      return c.commands[active]?.$.help
    }
  }

  const { exit } = useApp()

  if (stack.length === 0 || current === undefined) exit()

  function breadcrumbs() {
    return stack.filter(x => x.name !== ROOT).map(x => x.name).join(' ')
  }

  if (jump.length === 0) useEffect(() => void first_paint(), [])

  const section_length = !('commands' in current()) ? 0 : Object.keys((current() as HelpApp).commands).reduce((a, c) => a < c.length ? c.length : a, 0)

  return <Box flexDirection="column">
    {!('commands' in current()) ? Command({ ...current() as HelpCommand, name: breadcrumbs() }) : (
      <Box flexDirection="column" marginX={2} marginTop={1}>
        {breadcrumbs().trim().length === 0 ? <Fragment></Fragment> : <Text>Usage: <Text color='green'>{breadcrumbs()}</Text></Text>}
        {!current().description ? <Fragment></Fragment> : <Fragment>
          {breadcrumbs().trim().length === 0 ? <Fragment></Fragment> : <Box height={1}></Box>}
          <Text>{current().description}</Text>
        </Fragment>}
        {!current().help ? <Fragment></Fragment> : <Fragment>
          <Box height={1}></Box>
          <Text>{current().help}</Text>
        </Fragment>}
        {!('commands' in current()) && Object.keys((current() as HelpApp).commands).length === 0 ? <Fragment></Fragment> : <Fragment>
          <Box height={2}></Box>
          <Text underline={true}>Commands</Text>
          <Box marginLeft={2} marginTop={1} flexDirection="column">
            {Object.entries((current() as HelpApp).commands).map(([key, value]) => (
              <Section key={key} section={value.$} active={() => { }} focused={active} key_length={section_length} nocursor={nocursor}></Section>
            ))}
          </Box>
        </Fragment>}
        {!next_explain() ? <Fragment></Fragment> : <Fragment>
          <Box height={2}></Box>
          <Text underline={true}>Description</Text>
          <Box height={1}></Box>
          <Text>{next_explain()}</Text>
        </Fragment>}
      </Box>
    )}
    <Box marginTop={2} marginX={2}>
      <Text color='gray'>{"Navigation: Back <"}<Text color='magenta'>{"left arrow / backspace"}</Text>{">, Select <"}<Text color='magenta'>{"up arrow / down arrow"}</Text>{">, Enter <"}<Text color='magenta'>{"right arrow / return"}</Text>{">, Exit <"}<Text color='magenta'>{"esc"}</Text>{">"}</Text>
    </Box>
  </Box>
}


const wait = (ms: number) => new Promise(k => setTimeout(k, ms))

/**
 * Render the help menu.
 * 
 * @param print_only When set to true, prompt will print once and exit.
 * @param app Application to show help for.
 * @param jump Jump to subcommand on load.
 * @param renderer Renderer to use.
 */
export async function help(print_only: boolean, app: HelpApp | HelpCommand, jump: string[] = [], renderer = render) {
  let rendered = false
  const first_paint = () => {
    rendered = true
  }
  const x = () => {
    return <Help app={app} jump={jump} first_paint={first_paint} nocursor={print_only}></Help>
  }
  if (print_only) {
    const n = renderer(x())
    while (!rendered) {
      await wait(25)
    }
    n.unmount()
    return
  } else {
    const app = renderer(x())
    return app
  }
}