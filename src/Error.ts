/**
 * CatchableError
 * 
 * The main error object which all errors in the app extends from.
 * 
 * If an error is thrown extending this class, it will be caught by the cli class and printed as a message without stack traces.
 */
export abstract class CatchableError extends Error {
  /**
   * Create new CatchableError
   * 
   * @param message Message to print.
   */
  constructor(message: string) {
    super(message)
  }
}