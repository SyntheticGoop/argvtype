import type { Argument, Arguments } from './Arguments';
import type { Command } from './Command';
import { CatchableError } from './Error';
import type { AllowedLetters, Flag } from './Flag';
import { ROOT } from './singletons/ROOT';
import { UNSET } from './singletons/UNSET';


class ArgparseErrorUnrecoginsedFlag extends CatchableError {
  constructor(readonly flag: string) {
    super(`Flag ${flag} is unrecognized`)
  }
}
class ArgparseErrorMissingFlagArgument extends CatchableError {
  constructor(flag: string) {
    super(`Flag ${flag} is missing its argument`)
  }
}


class ArgparseErrorNonBooleanFlagConcat extends CatchableError {
  constructor(readonly flag: string, readonly flags: string) {
    super(`Flag ${flag} on initial part of flags ${flags} is not a boolean flag`)
  }
}

class ArgparseErrorMissingRequiredPositionalArgument extends CatchableError {
  constructor(readonly arg: string) {
    super(`Argument "${arg}" is required but not present`)
  }
}


const SHORT_FLAG = /^-(?=[^-@~\s])/

const LONG_FLAG = /^--(?=[^-@~\s])/

/**
 * Constrains an argument list against the number of arguments provided.
 * 
 * This function will return a 1-1 mapping of argument schemas to argument length so you can just sequentially parse
 * the arguments through the returned schema.
 * 
 * @param length Length of provided arguments.
 * @param args Argument array.
 */
function constrain_args(length: number, args: Array<Argument<any, any, any, any, any, any, any>>) {
  /* Is variadic */
  if (args.some(x => x.$.variadic)) {
    const required = args.filter(x => !x.$.variadic).length
    let optional = length - required
    const present_args: typeof args = []
    for (let i = 0; i < args.length; i++) {
      const a = args[i]
      if (!a) break
      if (!a.$.variadic) {
        present_args.push(a)
      } else {
        while (optional-- > 0) {
          present_args.push(a)
        }
      }
    }
    return present_args
  }
  else {
    const required = args.filter(x => !x.$.optional).length
    let optional = length - required
    const present_args: typeof args = []
    const missing_args: typeof args = []
    for (let i = 0; i < args.length; i++) {
      const a = args[i]
      if (!a) break
      if (!a.$.optional || (optional-- > 0)) {
        present_args.push(a)
      } else {
        missing_args.push(a)
      }
    }
    return present_args.concat(missing_args)
  }
}

/**
 * Extracts flags from a list of arguments based on provided schema.
 * 
 * Once flags are extracted, the argument list is returned with the arguments sans flags for argument parsing.
 * 
 * @param args Raw argument list.
 * @param flags Array of flag schemas.
 */
function parse_flags(
  args: string[],
  flags: Array<
    Flag<
      string,
      AllowedLetters | null,
      string | null,
      string | null,
      any,
      boolean,
      string[] | null,
      boolean
    >
  >
) {
  const extracted_flags: Record<string, any> = {}

  args = args.concat()

  const flagmap = new globalThis.Map<string, typeof flags extends Array<infer T> ? T : never>()
  for (const f of flags) {
    flagmap.set(f.$.name, f)
    if (f.$.default !== UNSET) extracted_flags[f.$.name] = f.$.default
    if (f.$.flag) flagmap.set(f.$.flag, f)
  }

  args = args.concat()
  const final_args: typeof args = []
  while (args.length > 0) {
    let arg = args.shift()
    if (!arg) break
    if (SHORT_FLAG.test(arg)) {
      const raw_flags = Array.from(arg.slice(1))
      const flags = []

      while (raw_flags.length > 0) {
        const flag = raw_flags.shift()
        if (!flag) break
        if (!flagmap.has(flag)) {
          raw_flags.unshift(flag)
          break
        }
        flags.push(flag)
      }


      for (const name of flags.slice(0, -1)) {
        const flag = flagmap.get(name)
        if (!flag) throw new ArgparseErrorUnrecoginsedFlag(arg.slice(1))
        if (!flag.$.boolean) throw new ArgparseErrorNonBooleanFlagConcat(name, arg.slice(1))
        extracted_flags[flag.$.name] = flag.$.invert ? false : true
      }


      const name = flags.slice(-1)[0]
      const flag = flagmap.get(name)
      if (!flag) throw new ArgparseErrorUnrecoginsedFlag(arg.slice(1))
      if (flag.$.boolean) {
        extracted_flags[flag.$.name] = flag.$.invert ? false : true
      } else {
        if (args.length === 0 && raw_flags.length === 0) throw new ArgparseErrorMissingFlagArgument(arg.slice(1))
        const val = raw_flags.length > 0 ? raw_flags.join('') : args.shift()!
        const coerced = flag.$.typed?.cast ? flag.$.typed.cast(val) : val
        extracted_flags[flag.$.name] = flag.$.typed?.variadic ? [...extracted_flags[flag.$.name] ?? [], coerced] : coerced

        while (flag.$.typed?.variadic && args.length > 0) {
          const arg = args.shift()
          if (!arg) break
          if (arg.startsWith('-')) {
            if (arg !== '-') args.unshift(arg)
            break
          }
          const coerced = flag.$.typed.cast ? flag.$.typed.cast(arg) : arg
          extracted_flags[flag.$.name] = [...extracted_flags[flag.$.name] ?? [], coerced]
        }
      }

    } else if (LONG_FLAG.test(arg)) {
      const name = arg.slice(2)
      const flag = flagmap.get(name)
      if (!flag) throw new ArgparseErrorUnrecoginsedFlag(arg.slice(1))
      if (flag.$.boolean) {
        extracted_flags[flag.$.name] = flag.$.invert ? false : true
      } else {
        if (args.length === 0) throw new ArgparseErrorMissingFlagArgument(name)
        const val = args.shift()!
        const coerced = flag.$.typed?.cast ? flag.$.typed.cast(val) : val
        extracted_flags[flag.$.name] = flag.$.typed?.variadic ? [...extracted_flags[flag.$.name] ?? [], coerced] : coerced

        while (flag.$.typed?.variadic && args.length > 0) {
          const arg = args.shift()
          if (!arg) break
          if (arg.startsWith('-')) {
            if (arg !== '-') args.unshift(arg)
            break
          }
          const coerced = flag.$.typed.cast ? flag.$.typed.cast(arg) : arg
          extracted_flags[flag.$.name] = [...extracted_flags[flag.$.name] ?? [], coerced]
        }
      }


    } else {
      final_args.push(arg)
    }
  }

  return {
    args: final_args,
    flags: extracted_flags
  }
}

/**
 * Parses and validates the command arguments and flags.
 * 
 * Returns the validated arguments and flags.
 * 
 * @param args Raw argument list.
 * @param cmd Command schema.
 */
export function argparse<C extends Command<
  string | typeof ROOT,
  string | null,
  Record<
    string,
    Flag<
      string,
      AllowedLetters | null,
      string | null,
      string | null,
      any,
      boolean,
      string[] | null,
      boolean
    >
  >,
  any,
  any,
  any,
  Arguments<Array<Argument<any, any, any, any, any, any, any>>>
>>(
  args: string[],
  cmd: C,
): { flags: Record<string, any>, args: Record<string, any> } {

  const options = {
    flags: {} as Record<string, any>,
    args: {} as Record<string, any>,
  };

  for (const arg of cmd.$.args.$.args) {
    if (arg.$.default !== UNSET) options.args[arg.$.name] = arg.$.default
  }

  const x = parse_flags(args, Object.values(cmd.$.flags))
  args = x.args
  options.flags = x.flags

  const constrained_args = constrain_args(args.length, cmd.$.args.$.args)
  for (let i = 0; i < constrained_args.length; i++) {
    const cfg = constrained_args[i]
    if (!cfg.$.optional && !cfg.$.variadic && i >= args.length) throw new ArgparseErrorMissingRequiredPositionalArgument(cfg.$.name)
    options.args[cfg.$.name] = cfg.$.variadic ?
      [...options.args[cfg.$.name] ?? [], ...(cfg.$.cast ? [cfg.$.cast(args[i])] : (i < args.length ? [args[i]] : cfg.$.default !== UNSET ? cfg.$.default : [null]))] :
      cfg.$.cast ? cfg.$.cast(args[i]) : (i < args.length ? args[i] : (cfg.$.default !== UNSET ? cfg.$.default : null))
  }

  return options
}
