/// <reference types="@types/jest"/>
import { cli, Group } from '../main'
import { MockRenderer } from '../MockRenderer'

function setup() {
  const actions = {
    copy: {
      file: jest.fn(async x => { }),
      merge: jest.fn(async x => { }),
    },
    list: jest.fn(async x => x),
  }
  const cmd = new Group()
    .description(`Manage system files and folders with this command line tool`)
    .help(`If possible, this application will use system commands to run its tasks.
The list of possible options are designed to emulate linux commands across other Operating Systems`)
    .group('copy', g => g
      .description('Copy files and folders.')
      .help('Attempt to copy files using the following: rsync, cp, node/fs\nIf copy fails, all files will be reverted.')
      .command('file', c => c
        .description('Copy files')
        .help('Copy multiple files to a single destination.')
        .example('Copy files without outputing to the console and force override destination files', '-l silent -F file1 file2 file3 ../file4 ./destination')

        .flag('loglevel', f => f
          .description('Set the logging level when copying')
          .enum('silent', 'progress', 'verbose')
          .letter('l')
          .default('progress')
          .optional()
        )
        .flag('force', f => f
          .letter('F')
          .description('Force copy even if destination exists.\nBy default, entire operation will be aborted.')
          .default(false)
          .optional()
        )
        .flag('include', f => f
          .description('Regular expressions that the filename must match in order for the copy to pass')
          .type('regexp', input => RegExp(input))
          .variadic()
          .optional()
        )
        .flag('delete', d => d
          .description('Deletes source files after successful copy of all files.')
          .default(false)
          .optional()
        )
        .arguments(arg => arg
          .arg('source', a => a
            .description('Source files to copy')
            .type('path')
            .variadic()
            .required()
          )
          .arg('dest', a => a
            .description('Destination folder to copy files to')
            .type('path')
            .required()
          )
        )
        .action(actions.copy.file)
      )
      .command('merge', c => c
        .description('Recursively merge folders')
        .help('Copies folders recursively, merging multiple sources into one destination.\nIf copy fails, files will remain.')
        .example('Copy one folder into another', 'source1 dest1')
        .example('Copy multiple folders into one', 'source1 source2 dest1')
        .example('Copy multiple folders into their own folders within a folder', 'source1 source2 source3 dest1/')
        .flag('loglevel', f => f
          .description('Set the logging level when copying')
          .enum('silent', 'progress', 'verbose')
          .letter('l')
          .default('progress')
          .optional()
        )
        .flag('force', f => f
          .letter('F')
          .description('Force copy even if destination exists.\nBy default, entire operation will be aborted.')
          .default(false)
          .optional()
        )
        .flag('include', f => f
          .description('Regular expressions that the filename must match in order for the copy to pass')
          .type('regexp', input => RegExp(input))
          .variadic()
          .optional()
        )
        .flag('delete', d => d
          .description('Deletes source files after successful copy of all files.')
          .default(false)
          .optional()
        )
        .arguments(arg => arg
          .arg('source', a => a
            .description('Source files to copy')
            .type('path')
            .variadic()
            .optional()
          )
          .arg('dest', a => a
            .description('Destination folder to copy files to')
            .type('path')
            .required()
          )
        )
        .action(actions.copy.merge)
      )
    )
    .command('list', c => c
      .description('List files')
      .help('List all files within selected folders.')
      .example('List one folder', 'folder1')
      .example('List multiple folders', 'folder1 folder2')
      .example('Recursively list all nested files', 'folder1 folder2 -R')
      .example('List files but filter output with regular expression', '-f "/[a-zA-Z]{3-5}.*" folder1')
      .arguments(args => args
        .arg('folders', a => a
          .description('Folders to list')
          .variadic()
          .required()
        )
      )
      .flag('filter', f => f
        .letter('f')
        .description('Filter results with regular expression')
        .variadic()
        .optional()
        .type('regexp', x => RegExp(x))
      )
      .flag('recursive', f => f
        .letter('r')
        .description('Recursively lists files')
        .default(false)
        .optional()
      )
      .action(actions.list)
    )

  const renderer = new MockRenderer()
  const log = jest.fn((..._args: any[]) => { })
  return {
    cmd,
    actions,
    renderer,
    cli: cli.bind({ log, renderer: renderer.render as any, noexit: true }),
    log,
  }
}

const tests: Array<[Command: string, Action: (action: ReturnType<typeof setup>['actions']) => jest.Mock, Result: any]> = [
  [
    'copy file s1 d1',
    x => x.copy.file,
    {
      args: {
        source: ['s1'],
        dest: 'd1',
      },
      flags: {
        delete: false,
        force: false,
        loglevel: 'progress'
      }
    }
  ],
  [
    'copy file s1 s2 s3 d1',
    x => x.copy.file,
    {
      args: {
        source: ['s1', 's2', 's3'],
        dest: 'd1'
      },
      flags: {
        delete: false,
        force: false,
        loglevel: 'progress'
      }
    }
  ],
  [
    'copy file s1 s2 s3 d1 --delete',
    x => x.copy.file,
    {
      args: {
        source: ['s1', 's2', 's3'],
        dest: 'd1'
      },
      flags: {
        delete: true,
        force: false,
        loglevel: 'progress'
      }
    }
  ],
  [
    'copy file -Fl silent s1 s2 s3 d1',
    x => x.copy.file,
    {
      args: {
        source: ['s1', 's2', 's3'],
        dest: 'd1'
      },
      flags: {
        delete: false,
        force: true,
        loglevel: 'silent'
      }
    }
  ],
  [
    'copy file -l silent s1 s2 -F s3 --delete d1',
    x => x.copy.file,
    {
      args: {
        source: ['s1', 's2', 's3'],
        dest: 'd1'
      },
      flags: {
        delete: true,
        force: true,
        loglevel: 'silent'
      }
    }
  ],
  [
    'copy merge s1 --include a b c - s2 --include d e - s3 d1 --include f g',
    x => x.copy.merge,
    {
      args: {
        source: ['s1', 's2', 's3'],
        dest: 'd1'
      },
      flags: {
        delete: false,
        force: false,
        loglevel: 'progress',
        include: [/a/, /b/, /c/, /d/, /e/, /f/, /g/]
      }
    }
  ],
]

const error: Array<[Command: string]> = [
  [''],
  ['--help'],
  ['copy --help'],
  ['copy'],
  ['copy file --help'],
  ['copy file'],
  ['copy merge --help'],
  ['copy merge'],
  ['list --help'],
]


it.each(tests)('Runs "%s"', async (args, action, result) => {
  const { cmd, actions, cli } = setup()
  await cli(args.split(/ /g), cmd)
  expect(action(actions)).toBeCalledWith(result)
})

it.each(error)('Errors on "%s"', async (args) => {
  const { cmd, renderer, cli, log } = setup()

  await cli(args.split(/ /g), cmd)
  await renderer.result
  await new Promise(k => setTimeout(k, 1000))
  expect((await renderer.result).lastFrame()).toMatchSnapshot()
  expect(log.mock.calls).toMatchSnapshot()
})