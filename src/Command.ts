import type { Argument } from './Arguments'
import { Arguments } from './Arguments'
import { CatchableError } from './Error'
import { Flag } from './Flag'
import { ROOT } from './singletons/ROOT'

export class ArgparseErrorUnrecoginsedFlag extends CatchableError {
  constructor(readonly flag: string) {
    super(`${flag} is unrecognized`)
  }
}

export class ArgparseErrorIncompleteFlag extends CatchableError {
  constructor() {
    super('Flag provided as - without letter')
  }
}

/**
 * Command
 * 
 * Holds the command schema.
 * 
 * A command is the a self contained executable function.
 */
export class Command<
  Name extends string | typeof ROOT = typeof ROOT,
  Desc extends string | null = null,
  Flags extends Record<string, Flag<any, any, any, any, any, any, any, any>> = {},
  Help extends string | null = null,
  Examples extends Array<{ explain: string, example: string }> = [],
  Action extends (config: {
    flags: {
      [K in keyof Flags]: Flags[K] extends Flag<any, any, any, any, infer T, infer U, any, any, infer V> ?
      (U extends true ?
        (V extends true ? T[] | undefined : T[]) :
        (V extends true ? T | undefined : T))
      : never
    },
    args: {
      [K in keyof Args['$']['keyargs']]: Args['$']['keyargs'][K] extends Argument<any, any, infer T, infer U, infer V, any, any> ?
      (U extends true ?
        (V extends true ? T[] | undefined : T) :
        (V extends true ? T | undefined : T))
      : never
    }
  }) => Promise<void> = () => Promise<void>,
  Args extends Arguments<any> = Arguments<[]>
  > {

  /**
   * Create new Root Command.
   */
  constructor()
  /**
   * Create new Named Command.
   * 
   * @param name Name of command.
   */
  constructor(
    name: Name,
  )
  /**
   * Create new Named Command.
   * 
   * @param name Name of command.
   * @param description Short (1 line) description of the command.
   * @param flags Flags of the command.
   * @param help Detailed help for the command.
   * @param examples List of usage examples for the command.
   * @param action Action function the command will execute.
   * @param args List of arguments for the command.
   */
  constructor(
    name: Name,
    description: Desc,
    flags: Flags,
    help: Help,
    examples: Examples,
    action: Action,
    args: Args
  )
  constructor(
    private _name: Name = ROOT as Name,
    private _description: Desc = null as Desc,
    private _flags: Flags = {} as Flags,
    private _help: Help = null as Help,
    private _examples: Examples = [] as unknown as [...Examples],
    private _action: Action = (async (_config: { flags: { [K in keyof Flags]: Flags[K] extends Flag<any, any, any, any, infer T, infer U> ? (U extends true ? T[] : T) : never }, args: { [K in keyof Args['$']]: Args['$'][K] extends Argument<any, any, infer T, infer U> ? (U extends true ? T[] : T) : never } }) => { }) as Action,
    private _args: Args = new Arguments() as Args
  ) {
    this.description = this.description.bind(this)
    this.example = this.example.bind(this)
    this.help = this.help.bind(this)
    this.addFlag = this.addFlag.bind(this)
  }

  /**
   * Set the name of the command.
   * 
   * @param name Unique name of command.
   */
  public name<Name extends string>(name: Name) {
    return new Command(name, this._description, this._flags, this._help, this._examples, this._action, this._args)
  }

  /**
   * Set the description of the command.
   * 
   * @param description Short (1 line) description of the command.
   */
  public description<Desc extends string | null>(description: Desc) {
    return new Command(this._name, description, this._flags, this._help, this._examples, this._action, this._args)
  }

  /**
   * Add an example for the command usage.
   * 
   * @param explain Explaination of what this command usage does.
   * @param example Command arguments and flags.
   */
  public example<Explain extends string, Example extends string>(explain: Explain, example: Example) {
    return new Command(this._name, this._description, this._flags, this._help, [...this._examples, { explain, example }] as [...Examples, { explain: Explain, example: Example }], this._action, this._args)
  }

  /**
   * Add arguments to the command via a callback.
   * 
   * The callback will provide the arguments object to chain the setup.
   * The returned value of the callback will be set to the command.
   * 
   * @param args Arguments creation callback function.
   */
  public arguments<AddArgs extends Arguments<any>>(args: (args: Args) => AddArgs) {
    return new Command(
      this._name,
      this._description,
      this._flags,
      this._help,
      this._examples,
      this._action as unknown as (config: {
        flags: {
          [K in keyof Flags]: Flags[K] extends Flag<any, any, any, any, infer T, infer U, any, any, infer V> ?
          (U extends true ?
            (V extends true ? T[] | undefined : T[]) :
            (V extends true ? T | undefined : T))
          : never
        },
        args: {
          [K in keyof AddArgs['$']['keyargs']]: AddArgs['$']['keyargs'][K] extends Argument<any, any, infer T, infer U, infer V, any, any> ?
          (U extends true ?
            (V extends true ? T[] | undefined : T) :
            (V extends true ? T | undefined : T))
          : never
        }
      }) => Promise<void>,
      args(this._args)
    )
  }

  /**
   * Add arguments to the command.
   * 
   * @param args Arguments to add.
   */
  public addArguments<Args extends Arguments<any>>(args: Args) {
    return new Command(
      this._name,
      this._description,
      this._flags,
      this._help,
      this._examples,
      this._action as unknown as (config: {
        flags: {
          [K in keyof Flags]: Flags[K] extends Flag<any, any, any, any, infer T, infer U, any, any, infer V> ?
          (U extends true ?
            (V extends true ? T[] | undefined : T[]) :
            (V extends true ? T | undefined : T))
          : never
        },
        args: {
          [K in keyof Args['$']['keyargs']]: Args['$']['keyargs'][K] extends Argument<any, any, infer T, infer U, infer V, any, any> ?
          (U extends true ?
            (V extends true ? T[] | undefined : T) :
            (V extends true ? T | undefined : T))
          : never
        }
      }) => Promise<void>,
      args
    )
  }

  /**
   * Set the help of the command.
   * 
   * @param help Detailed help for the command.
   */
  public help<Help extends string | null>(help: Help) {
    return new Command(this._name, this._description, this._flags, help, this._examples, this._action, this._args)
  }

  /**
   * Add a flag to the command via a callback.
   * 
   * The callback will provide the flag object to chain the setup.
   * The returned value of the callback will be added to the command.
   * 
   * @param name Name of the flag.
   * @param flag Flag creation callback function.
   */
  public flag<Name extends string, F extends Flag<any, any, any, any, any, any, any, any, any>>(name: Name, flag: (flag: Flag<Name>) => F) {
    return this.addFlag(flag(new Flag(name)), name)
  }

  /**
   * Add flag to the command.
   * 
   * @param args Flag to add.
   */
  public addFlag<AddName extends string, F extends Flag<AddName, any, any, any, any, any, any, any, any>>(flag: F, _name?: AddName) {
    return new Command(this._name, this._description, { ...this._flags, [flag.$.name]: flag } as unknown as Flags & { [i in AddName]: F }, this._help, this._examples, this._action, this._args)
  }

  /**
   * Bind action callback to the command.
   * 
   * The action callback arguments are automatically inferred from the command object.
   * 
   * @param action Action to execute for the command.
   */
  public action<Action extends (config: {
    flags: {
      [K in keyof Flags]: Flags[K] extends Flag<any, any, any, any, infer T, infer U, any, any, infer V> ?
      (U extends true ?
        (V extends true ? T[] | undefined : T[]) :
        (V extends true ? T | undefined : T))
      : never
    },
    args: {
      [K in keyof Args['$']['keyargs']]: Args['$']['keyargs'][K] extends Argument<any, any, infer T, infer U, infer V, any, any> ?
      (U extends true ?
        (V extends true ? T[] | undefined : T) :
        (V extends true ? T | undefined : T))
      : never
    }
  }) => Promise<void>>(action: Action) {
    return new Command(this._name, this._description, this._flags, this._help, this._examples, action, this._args)
  }

  /** Command configuration */
  get $() {
    return {
      name: this._name,
      description: this._description,
      flags: this._flags,
      help: this._help,
      examples: this._examples,
      action: this._action,
      args: this._args
    }
  }
}

