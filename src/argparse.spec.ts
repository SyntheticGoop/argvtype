/// <reference types="@types/jest"/>
import { argparse } from './argparse'
import { Command } from './Command'
const passing: Array<[Test: string, Arguments: string[], Command: Command<any, any, any, any, any, any, any>, Result: { args: Record<string, any>, flags: Record<string, any> }]> = [
  [
    'Parse no arguments',
    [],
    new Command(''),
    {
      args: {},
      flags: {},
    }
  ],
  [
    'Parse 1 required argument',
    ['arg1'],
    new Command('')
      .arguments(a => a
        .arg('a', x => x)
      ),
    {
      args: {
        a: 'arg1'
      },
      flags: {}
    }
  ],
  [
    'Parse 2 required arguments',
    ['arg1', 'arg2'],
    new Command('')
      .arguments(a => a
        .arg('a', x => x)
        .arg('b', x => x.coerce(x => x + ':cast'))
      ),
    {
      args: {
        a: 'arg1',
        b: 'arg2:cast',
      },
      flags: {}
    }
  ],
  [
    'Parse 3 required arguments',
    ['arg1', 'arg2', 'arg3'],
    new Command('')
      .arguments(a => a
        .arg('a', x => x)
        .arg('b', x => x)
        .arg('c', x => x)
      ),
    {
      args: {
        a: 'arg1',
        b: 'arg2',
        c: 'arg3',
      },
      flags: {}
    }
  ],
  [
    'Parse 0/1 optional argument',
    [],
    new Command('')
      .arguments(a => a
        .arg('a', x => x.optional(true).coerce(x => x ?? 'default'))
      ),
    {
      args: {
        a: 'default'
      },
      flags: {}
    }
  ],
  [
    'Parse 0/1 optional argument',
    [],
    new Command('')
      .arguments(a => a
        .arg('a', x => x.optional().default('default'))
      ),
    {
      args: {
        a: 'default'
      },
      flags: {}
    }
  ],
  [
    'Parse 1/1 optional argument',
    ['arg1'],
    new Command('')
      .arguments(a => a
        .arg('a', x => x.optional(true))
      ),
    {
      args: {
        a: 'arg1'
      },
      flags: {}
    }
  ],
  [
    'Parse 0/2 optional arguments',
    [],
    new Command('')
      .arguments(a => a
        .arg('a', x => x.optional(true))
        .arg('b', x => x.optional(true).coerce(x => x ?? 'default'))
      ),
    {
      args: {
        a: null,
        b: 'default',
      },
      flags: {}
    }
  ],
  [
    'Parse 1/2 optional arguments',
    ['arg1'],
    new Command('')
      .arguments(a => a
        .arg('a', x => x.optional(true))
        .arg('b', x => x.optional(true))
      ),
    {
      args: {
        a: 'arg1',
        b: null,
      },
      flags: {}
    }
  ],
  [
    'Parse 2/2 optional arguments',
    ['arg1', 'arg2'],
    new Command('')
      .arguments(a => a
        .arg('a', x => x.optional(true))
        .arg('b', x => x.optional(true))
      ),
    {
      args: {
        a: 'arg1',
        b: 'arg2',
      },
      flags: {}
    }
  ],
  [
    'Parse 2 required, 0/2 optional arguments',
    ['arg1', 'arg2'],
    new Command('')
      .arguments(a => a
        .arg('a', x => x)
        .arg('b', x => x)
        .arg('c', x => x.optional(true))
        .arg('d', x => x.optional(true).coerce(x => x ?? 'default'))
      ),
    {
      args: {
        a: 'arg1',
        b: 'arg2',
        c: null,
        d: 'default'
      },
      flags: {}
    }
  ],
  [
    'Parse 2 required, 1/2 optional arguments',
    ['arg1', 'arg2', 'arg3'],
    new Command('')
      .arguments(a => a
        .arg('a', x => x)
        .arg('b', x => x)
        .arg('c', x => x.optional(true))
        .arg('d', x => x.optional(true).coerce(x => x ?? 'default'))
      ),
    {
      args: {
        a: 'arg1',
        b: 'arg2',
        c: 'arg3',
        d: 'default'
      },
      flags: {}
    }
  ],
  [
    'Parse 2 required, 2/2 optional arguments',
    ['arg1', 'arg2', 'arg3', 'arg4'],
    new Command('')
      .arguments(a => a
        .arg('a', x => x)
        .arg('b', x => x)
        .arg('c', x => x.optional(true))
        .arg('d', x => x.optional(true).coerce(x => x ?? 'default'))
      ),
    {
      args: {
        a: 'arg1',
        b: 'arg2',
        c: 'arg3',
        d: 'arg4'
      },
      flags: {}
    }
  ],
  [
    'Parse 2 required, 0/2 optional arguments reversed',
    ['arg3', 'arg4'],
    new Command('')
      .arguments(a => a
        .arg('a', x => x.optional(true))
        .arg('b', x => x.optional(true).coerce(x => x ?? 'default'))
        .arg('c', x => x)
        .arg('d', x => x)
      ),
    {
      args: {
        a: null,
        b: 'default',
        c: 'arg3',
        d: 'arg4',
      },
      flags: {}
    }
  ],
  [
    'Parse 2 required, 1/2 optional arguments reversed',
    ['arg1', 'arg3', 'arg4'],
    new Command('')
      .arguments(a => a
        .arg('a', x => x.optional(true))
        .arg('b', x => x.optional(true).coerce(x => x ?? 'default'))
        .arg('c', x => x)
        .arg('d', x => x)
      ),
    {
      args: {
        a: 'arg1',
        b: 'default',
        c: 'arg3',
        d: 'arg4',
      },
      flags: {}
    }
  ],
  [
    'Parse 2 required, 2/2 optional arguments reversed',
    ['arg1', 'arg2', 'arg3', 'arg4'],
    new Command('')
      .arguments(a => a
        .arg('a', x => x.optional(true))
        .arg('b', x => x.optional(true).coerce(x => x ?? 'default'))
        .arg('c', x => x)
        .arg('d', x => x)
      ),
    {
      args: {
        a: 'arg1',
        b: 'arg2',
        c: 'arg3',
        d: 'arg4',
      },
      flags: {}
    }
  ],
  [
    'Parse 2 required, 0/3 optional arguments interspersed',
    ['arg2', 'arg5'],
    new Command('')
      .arguments(a => a
        .arg('a', x => x.optional(true))
        .arg('b', x => x)
        .arg('c', x => x.optional(true).coerce(x => x ?? 'default'))
        .arg('d', x => x.optional(true))
        .arg('e', x => x)
      ),
    {
      args: {
        a: null,
        b: 'arg2',
        c: 'default',
        d: null,
        e: 'arg5'
      },
      flags: {}
    }
  ],
  [
    'Parse 2 required, 1/3 optional arguments interspersed',
    ['arg1', 'arg2', 'arg5'],
    new Command('')
      .arguments(a => a
        .arg('a', x => x.optional(true))
        .arg('b', x => x)
        .arg('c', x => x.optional(true).coerce(x => x ?? 'default'))
        .arg('d', x => x.optional(true))
        .arg('e', x => x)
      ),
    {
      args: {
        a: 'arg1',
        b: 'arg2',
        c: 'default',
        d: null,
        e: 'arg5'
      },
      flags: {}
    }
  ],
  [
    'Parse 2 required, 2/3 optional arguments interspersed',
    ['arg1', 'arg2', 'arg3', 'arg5'],
    new Command('')
      .arguments(a => a
        .arg('a', x => x.optional(true))
        .arg('b', x => x)
        .arg('c', x => x.optional(true).coerce(x => x ?? 'default'))
        .arg('d', x => x.optional(true))
        .arg('e', x => x)
      ),
    {
      args: {
        a: 'arg1',
        b: 'arg2',
        c: 'arg3',
        d: null,
        e: 'arg5'
      },
      flags: {}
    }
  ],
  [
    'Parse 2 required, 3/3 optional arguments interspersed',
    ['arg1', 'arg2', 'arg3', 'arg4', 'arg5'],
    new Command('')
      .arguments(a => a
        .arg('a', x => x.optional(true))
        .arg('b', x => x)
        .arg('c', x => x.optional(true).coerce(x => x ?? 'default'))
        .arg('d', x => x.optional(true))
        .arg('e', x => x)
      ),
    {
      args: {
        a: 'arg1',
        b: 'arg2',
        c: 'arg3',
        d: 'arg4',
        e: 'arg5'
      },
      flags: {}
    }
  ],
  [
    'Parse variadic argument',
    ['1', '2', '3'],
    new Command('')
      .arguments(a => a
        .arg('a', a => a.variadic(true).coerce(x => +x))
      ),
    {
      args: {
        a: [1, 2, 3]
      },
      flags: {}
    }
  ],
  [
    'Parse default variadic argument',
    [],
    new Command('')
      .arguments(a => a
        .arg('a', a => a.variadic(true).default([1, 2, 3]))
      ),
    {
      args: {
        a: [1, 2, 3]
      },
      flags: {}
    }
  ],
  [
    'Parse variadic argument before 3 required',
    ['1', '2', '3', 'arg1', 'arg2', 'arg3'],
    new Command('')
      .arguments(a => a
        .arg('a', a => a.variadic(true).coerce(x => +x))
        .arg('b', x => x)
        .arg('c', x => x)
        .arg('d', x => x)
      ),
    {
      args: {
        a: [1, 2, 3],
        b: 'arg1',
        c: 'arg2',
        d: 'arg3',
      },
      flags: {}
    }
  ],
  [
    'Parse variadic argument interspersed (1) 3 required',
    ['arg1', '1', '2', '3', 'arg2', 'arg3'],
    new Command('')
      .arguments(a => a
        .arg('a', x => x)
        .arg('b', a => a.variadic(true).coerce(x => +x))
        .arg('c', x => x)
        .arg('d', x => x)
      ),
    {
      args: {
        a: 'arg1',
        b: [1, 2, 3],
        c: 'arg2',
        d: 'arg3',
      },
      flags: {}
    }
  ],
  [
    'Parse variadic argument before 3 required',
    ['1', '2', '3', 'arg1', 'arg2', 'arg3'],
    new Command('')
      .arguments(a => a
        .arg('a', a => a.variadic(true).coerce(x => +x))
        .arg('b', x => x)
        .arg('c', x => x)
        .arg('d', x => x)
      ),
    {
      args: {
        a: [1, 2, 3],
        b: 'arg1',
        c: 'arg2',
        d: 'arg3',
      },
      flags: {}
    }
  ],
  [
    'Parse variadic argument interspersed (1) 3 required',
    ['arg1', 'arg2', '1', '2', '3', 'arg3'],
    new Command('')
      .arguments(a => a
        .arg('a', x => x)
        .arg('b', x => x)
        .arg('c', a => a.variadic(true).coerce(x => +x))
        .arg('d', x => x)
      ),
    {
      args: {
        a: 'arg1',
        b: 'arg2',
        c: [1, 2, 3],
        d: 'arg3',
      },
      flags: {}
    }
  ],
  [
    'Parse variadic argument after 3 required',
    ['arg1', 'arg2', 'arg3', '1', '2', '3'],
    new Command('')
      .arguments(a => a
        .arg('a', x => x)
        .arg('b', x => x)
        .arg('c', x => x)
        .arg('d', a => a.variadic(true).coerce(x => +x))
      ),
    {
      args: {
        a: 'arg1',
        b: 'arg2',
        c: 'arg3',
        d: [1, 2, 3],
      },
      flags: {}
    }
  ],
  [
    'Parse 1 short flag',
    ['-a', 'flag1'],
    new Command('')
      .flag('aaa', f => f
        .letter('a')
        .type('word', x => x)
      ),
    {
      args: {},
      flags: {
        aaa: 'flag1'
      }
    }
  ],
  [
    'Parse 1 long flag',
    ['--aaa', 'flag1'],
    new Command('')
      .flag('aaa', f => f
        .letter('a')
        .type('word', x => x)
      ),
    {
      args: {},
      flags: {
        aaa: 'flag1'
      }
    }
  ],
  [
    'Parse 1 short flag with value concatenated',
    ['-a1'],
    new Command('')
      .flag('aaa', f => f
        .letter('a')
        .type('number', x => +x)
      ),
    {
      args: {},
      flags: {
        aaa: 1
      }
    }
  ],
  [
    'Parse multiple concatenated boolean flags',
    ['-abc', '-d'],
    new Command('')
      .flag('aaa', f => f.letter('a').default(false))
      .flag('bbb', f => f.letter('b').default(true).invert())
      .flag('ccc', f => f.letter('c').default(false))
      .flag('ddd', f => f.letter('d').default(true).invert())
      .flag('eee', f => f.letter('e').default(false))
      .flag('fff', f => f.letter('f').default(true))
      .flag('ggg', f => f.letter('g')),
    {
      args: {},
      flags: {
        aaa: true,
        bbb: false,
        ccc: true,
        ddd: false,
        eee: false,
        fff: true,
      }
    }
  ],
  [
    'Parse multiple concatenated boolean flags with argument concatenated',
    ['-abcx'],
    new Command('')
      .flag('aaa', f => f.letter('a').default(false))
      .flag('bbb', f => f.letter('b').default(true).invert())
      .flag('ccc', f => f.letter('c').type('word', x => x)),
    {
      args: {},
      flags: {
        aaa: true,
        bbb: false,
        ccc: 'x',
      }
    }
  ],
  [
    'Parse multiple concatenated boolean flags with argument',
    ['-abc', 'x'],
    new Command('')
      .flag('aaa', f => f.letter('a').default(false))
      .flag('bbb', f => f.letter('b').default(true).invert())
      .flag('ccc', f => f.letter('c').type('word', x => x)),
    {
      args: {},
      flags: {
        aaa: true,
        bbb: false,
        ccc: 'x',
      }
    }
  ],
  [
    'Parse 2 flag',
    ['-a1', '-b', 'flag2'],
    new Command('')
      .flag('bbb', f => f
        .letter('b')
        .type('word', x => x)
      )
      .flag('aaa', f => f
        .letter('a')
        .type('number', x => +x)
      ),
    {
      args: {},
      flags: {
        aaa: 1,
        bbb: 'flag2',
      }
    }
  ],
  [
    'Parse 2 flag reverse',
    ['-b', 'flag2', '-a1'],
    new Command('')
      .flag('bbb', f => f
        .letter('b')
        .type('word', x => x)
      )
      .flag('aaa', f => f
        .letter('a')
        .type('number', x => +x)
      ),
    {
      args: {},
      flags: {
        aaa: 1,
        bbb: 'flag2',
      }
    }
  ],
  [
    'Parse 1/2 (1) optional flag',
    ['-b', 'flag2'],
    new Command('')
      .flag('bbb', f => f
        .letter('b')
        .type('word', x => x)
      )
      .flag('aaa', f => f
        .letter('a')
        .optional()
        .default(false)
      ),
    {
      args: {},
      flags: {
        aaa: false,
        bbb: 'flag2',
      }
    }
  ],
  [
    'Parse 1/2 (2) optional flag',
    ['-a1'],
    new Command('')
      .flag('bbb', f => f
        .letter('b')
        .default(false)
        .type('word', x => x)
      )
      .flag('aaa', f => f
        .letter('a')
        .optional(true)
        .type('number', x => +x)
      ),
    {
      args: {},
      flags: {
        aaa: 1,
        bbb: false,
      }
    }
  ],
  [
    'Parse variadic flag',
    ['-a1', '2', '3', '-a', '4'],
    new Command('')
      .flag('aaa', f => f
        .letter('a')
        .variadic(true)
        .type('number', x => +x)
      ),
    {
      args: {},
      flags: {
        aaa: [1, 2, 3, 4]
      }
    }
  ],
  [
    'Parse multiple concatenated boolean flags with variadic arguments',
    ['-abc', 'x', 'y', 'z'],
    new Command('')
      .flag('aaa', f => f.letter('a').default(false))
      .flag('bbb', f => f.letter('b').default(true).invert())
      .flag('ccc', f => f.letter('c').variadic().type('word', x => x)),
    {
      args: {},
      flags: {
        aaa: true,
        bbb: false,
        ccc: ['x', 'y', 'z'],
      }
    }
  ],
  [
    'Parse multiple concatenated boolean flags with variadic arguments concatenated',
    ['-abcx', 'y', 'z'],
    new Command('')
      .flag('aaa', f => f.letter('a').default(false))
      .flag('bbb', f => f.letter('b').default(true).invert())
      .flag('ccc', f => f.letter('c').variadic().type('word', x => x)),
    {
      args: {},
      flags: {
        aaa: true,
        bbb: false,
        ccc: ['x', 'y', 'z'],
      }
    }
  ],
  [
    'Parse flag before arguments',
    ['-a', '1', '-b2', 'arg1', 'arg2'],
    new Command('')
      .flag('aaa', f => f
        .letter('a')
        .type('number', x => +x)
      )
      .flag('bbb', f => f
        .letter('b')
        .type('number', x => +x)
      )
      .arguments(a => a
        .arg('a', x => x)
        .arg('b', x => x)
      ),
    {
      args: {
        a: 'arg1',
        b: 'arg2'
      },
      flags: {
        aaa: 1,
        bbb: 2,
      }
    }
  ],
  [
    'Parse flag after arguments',
    ['arg1', 'arg2', '-a', '1', '-b2'],
    new Command('')
      .flag('aaa', f => f
        .letter('a')
        .type('number', x => +x)
      )
      .flag('bbb', f => f
        .letter('b')
        .type('number', x => +x)
      )
      .arguments(a => a
        .arg('a', x => x)
        .arg('b', x => x)
      ),
    {
      args: {
        a: 'arg1',
        b: 'arg2'
      },
      flags: {
        aaa: 1,
        bbb: 2,
      }
    }
  ],
  [
    'Parse variadic flag before variadic arguments',
    ['-a', '1', '-b2', '3', '-', 'arg1', 'arg2', 'arg3'],
    new Command('')
      .flag('aaa', f => f
        .letter('a')
        .type('number', x => +x)
      )
      .flag('bbb', f => f
        .letter('b')
        .type('number', x => +x)
        .variadic(true)
      )
      .arguments(a => a
        .arg('a', x => x)
        .arg('b', x => x.variadic())
      ),
    {
      args: {
        a: 'arg1',
        b: ['arg2', 'arg3']
      },
      flags: {
        aaa: 1,
        bbb: [2, 3],
      }
    }
  ],
  [
    'Parse variadic argument before variadic flags',
    ['arg1', 'arg2', 'arg3', '-a1', '-b', '2', '3'],
    new Command('')
      .flag('aaa', f => f
        .letter('a')
        .type('number', x => +x)
      )
      .flag('bbb', f => f
        .letter('b')
        .type('number', x => +x)
        .variadic(true)
      )
      .arguments(a => a
        .arg('a', x => x)
        .arg('b', x => x.variadic(true))
      ),
    {
      args: {
        a: 'arg1',
        b: ['arg2', 'arg3']
      },
      flags: {
        aaa: 1,
        bbb: [2, 3],
      }
    }
  ]
]

const failing: Array<[Test: string, Arguments: string[], Command: Command<any, any, any, any, any, any, any>, Error: string]> = [
  [
    'Encounters missing required argument',
    ['a', 'b'],
    new Command('')
      .arguments(a => a
        .arg('a', x => x)
        .arg('b', x => x)
        .arg('c', x => x)
      ),
    'Argument "c" is required but not present'
  ],
  [
    'Encounters missing required argument in optionals',
    ['a'],
    new Command('')
      .arguments(a => a
        .arg('a', x => x.optional())
        .arg('b', x => x)
        .arg('c', x => x.optional())
        .arg('d', x => x)
      ),
    'Argument "d" is required but not present'
  ],
]


it.each(passing)('%s', (_, args, cmd, result) => {
  expect(argparse(args, cmd)).toEqual(result)
})

it.each(failing)('%s', (_, args, cmd, error) => {
  expect(() => argparse(args, cmd)).toThrowError(error)
})
