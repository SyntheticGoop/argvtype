import { Command } from '../../../main'
export default new Command()
  .description('Chicken rice')
  .help('Standard chicken rice. Hinanese style.')
  .flag('chilli', flag => flag.optional().default(false))
  .flag('ketchup', flag => flag.optional().default(false))
  .flag('white', flag => flag.optional().default(false))
  .example('Get chicken rice', '')