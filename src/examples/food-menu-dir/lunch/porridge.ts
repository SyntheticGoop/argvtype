import { Command } from '../../../main'
export default new Command()
  .description('Porridge')
  .help(`Poor man's porridge.`)
  .arguments(args => args
    .arg('type', arg => arg
      .description('Type of porridge')
      .enum('fish', 'chicken', 'pork')
      .required()
    )
  )