import { cmddir } from '../../../cmddir'
export default cmddir(__dirname)
  .description('The lunch menu.')
  .help('The standard lunch menu.\nPick from a standard set of meals.')


