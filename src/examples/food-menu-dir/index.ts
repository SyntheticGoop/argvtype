import { cmddir } from '../../cmddir'
import { cli } from '../../main'

const group = cmddir(__dirname)
  .name('menu')
  .description('Order some food from a menu.')
  .help(`With this command you can pick from multiple menus and decide on which foods to order.
The available menus are: Breakfast, Lunch, and Dinner.`)

cli(process.argv.slice(2), group)