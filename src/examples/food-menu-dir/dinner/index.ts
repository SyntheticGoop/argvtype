import { cmddir } from '../../../cmddir'
export default cmddir(__dirname)
  .description('The dinner menu.')
  .help('The standard dinner menu.\nPick from a standard set of meals or burgers.')


