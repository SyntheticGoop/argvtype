import { Argument, Arguments } from '../../../Arguments'
import { Command } from '../../../main'
export default new Command()
  .description('Great steaks')
  .help(`Choose from a variety of steaks`)
  .arguments(args => args
    .arg('cuts', arg => arg
      .description('Steak cuts')
      .enum('rib-eye', 'sirloin', 't-bone')
      .required()
    )
  )
  .flag('doneness', flag => flag
    .description('The doneness of the sausage')
    .letter('d')
    .enum('raw', 'medium-rare', 'medium-well', 'well-done')
    .optional()
  )