import { cmddir } from '../../../../cmddir'
export default cmddir(__dirname)
  .description('Many great burgers.')
  .help('An assortment of burgers.')