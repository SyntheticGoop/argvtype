import { Command, CatchableError } from '../../../main'

class FiniteNumber extends CatchableError {
  constructor(value: any) {
    super(`Parsed value: ${value} does not cast into a valid number!`)
  }
}

export default new Command()
  .description('Sausages in various styles and flavours.')
  .help(`Sausages are a common source of protein in breakfasts.
Choosing the right sausage could very well make your day!`)
  .arguments(args => args
    .arg('type', arg => arg
      .description('Style of sausage')
      .enum('Bratwurst', 'Lap cheong', 'Vienna')
      .type('style', x => x)
    )
    .arg('quantity', arg => arg
      .description('Quantity of sausages')
      .optional()
      .type('number', x => {
        if (!x) throw new FiniteNumber(x)
        const parsed = +x
        if (!Number.isFinite(parsed)) throw new FiniteNumber(x)
        return parsed
      })
      .default(3)
    )
  )
  .flag('doneness', flag => flag
    .description('The doneness of the sausage')
    .letter('d')
    .enum('raw', 'medium-rare', 'medium-well', 'well-done')
    .optional()
  )