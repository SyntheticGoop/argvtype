import { cmddir } from '../../../cmddir'

export default cmddir(__dirname)
  .description('The breakfast menu.')
  .help(`The breakfast menu is for early risers.
For breakfast, you choose the eggs and sausages you want.`)
