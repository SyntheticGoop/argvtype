import { Command, CatchableError } from '../../../main'

class FiniteNumber extends CatchableError {
  constructor(value: any) {
    super(`Parsed value: ${value} does not cast into a valid number!`)
  }
}

export default new Command()
  .description('Egg selection')
  .help(`Do you like your eggs sunny side up, scrambled or half boiled?`)
  .arguments(args => args
    .arg('type', arg => arg
      .description('Style of egg')
      .enum('sunny', 'scrambled', 'half')
      .type('style', x => x)
    )
    .arg('quantity', arg => arg
      .description('Quantity of sausages')
      .optional()
      .type('number', x => {
        if (!x) throw new FiniteNumber(x)
        const parsed = +x
        if (!Number.isFinite(parsed)) throw new FiniteNumber(x)
        return parsed
      })
      .default(3)
    )
  )