
import { CatchableError, cli, Group } from '../main'

class FiniteNumber extends CatchableError {
  constructor(value: any) {
    super(`Parsed value: ${value} does not cast into a valid number!`)
  }
}
const cmd = new Group('menu')
  .description('Order some food from a menu.')
  .help(`With this command you can pick from multiple menus and decide on which foods to order.
The available menus are: Breakfast, Lunch, and Dinner.`)

  .group('breakfast', group => group
    .description('The breakfast menu.')
    .help(`The breakfast menu is for early risers.
For breakfast, you choose the eggs and sausages you want.`)

    .command('sausage', cmd => cmd
      .description('Sausages in various styles and flavours.')
      .help(`Sausages are a common source of protein in breakfasts.
Choosing the right sausage could very well make your day!`)
      .arguments(args => args
        .arg('type', arg => arg
          .description('Style of sausage')
          .enum('Bratwurst', 'Lap cheong', 'Vienna')
          .type('style', x => x)
        )
        .arg('quantity', arg => arg
          .description('Quantity of sausages')
          .optional()
          .type('number', x => {
            if (!x) throw new FiniteNumber(x)
            const parsed = +x
            if (!Number.isFinite(parsed)) throw new FiniteNumber(x)
            return parsed
          })
          .default(3)
        )
      )
      .flag('doneness', flag => flag
        .description('The doneness of the sausage')
        .letter('d')
        .enum('raw', 'medium-rare', 'medium-well', 'well-done')
        .optional()
      )
    )

    .command('eggs', cmd => cmd
      .description('Egg selection')
      .help(`Do you like your eggs sunny side up, scrambled or half boiled?`)
      .arguments(args => args
        .arg('type', arg => arg
          .description('Style of egg')
          .enum('sunny', 'scrambled', 'half')
          .type('style', x => x)
        )
        .arg('quantity', arg => arg
          .description('Quantity of sausages')
          .optional()
          .type('number', x => {
            if (!x) throw new FiniteNumber(x)
            const parsed = +x
            if (!Number.isFinite(parsed)) throw new FiniteNumber(x)
            return parsed
          })
          .default(3)
        )
      )
    )
  )

  .group('lunch', group => group
    .description('The lunch menu.')
    .help('The standard lunch menu.\nPick from a standard set of meals.')

    .command('porridge', cmd => cmd
      .description('Porridge')
      .help(`Poor man's porridge.`)
      .arguments(args => args
        .arg('type', arg => arg
          .description('Type of porridge')
          .enum('fish', 'chicken', 'pork')
          .required()
        )
      )
    )

    .command('chicken-rice', cmd => cmd
      .description('Chicken rice')
      .help('Standard chicken rice. Hinanese style.')
      .flag('chilli', flag => flag.optional().default(false))
      .flag('ketchup', flag => flag.optional().default(false))
      .flag('white', flag => flag.optional().default(false))
      .example('Get chicken rice', '')
    )
  )

  .group('dinner', group => group
    .description('The dinner menu.')
    .help('The standard dinner menu.\nPick from a standard set of meals or burgers.')

    .command('steak', cmd => cmd
      .description('Great steaks')
      .help(`Choose from a variety of steaks`)
      .arguments(args => args
        .arg('cuts', arg => arg
          .description('Steak cuts')
          .enum('rib-eye', 'sirloin', 't-bone')
          .required()
        )
      )
      .flag('doneness', flag => flag
        .description('The doneness of the sausage')
        .letter('d')
        .enum('raw', 'medium-rare', 'medium-well', 'well-done')
        .optional()
      )
    )

    .group('burger', group => group
      .description('Many great burgers.')
      .help('An assortment of burgers.')

      .command('quarter-pounder', cmd => cmd
        .description('For the meat lovers.')
        .help('A heavy duty solution.')
      )

      .command('cheeseburger', cmd => cmd
        .description('American cheesebruger.')
        .help('For the cheese lovers.')
      )

      .command('quarter-pounder', cmd => cmd
        .description('Your daily intake of fish')
        .help('Not very healthy.')
      )
    )
  )

cli(process.argv.slice(2), cmd)