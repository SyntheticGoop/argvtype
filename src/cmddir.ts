import { readdirSync } from 'fs'
import { resolve } from 'path'
import { Command } from './Command'
import { Group } from './Group'

/**
 * Recursively searches and picks up commands from the calling directory.
 * 
 * In order to use directory discovery, each folder containing commands must have an index
 * file with a group linking all the subcommands (can be another dir function).
 * The index file must export the group as an ES6 default export, or as a CommonJS exports.default value.
 * 
 * @returns Group object
 */
export function cmddir(): Group<any, any, any, any, any>
/**
 * Recursively searches and picks up commands from a directory as subcommands.
 * 
 * In order to use directory discovery, each folder containing commands must have an index
 * file with a group linking all the subcommands (can be another dir function).
 * The index file must export the group as an ES6 default export, or as a CommonJS exports.default value.
 * 
 * @param root Root folder to begin searching.
 * @returns Group object
 */
export function cmddir(root: string): Group<any, any, any, any, any>
/**
 * Recursively searches and picks up commands from a directory as subcommands, with the group object as the root command.
 * 
 * In order to use directory discovery, each folder containing commands must have an index
 * file with a group linking all the subcommands (can be another dir function).
 * The index file must export the group as an ES6 default export, or as a CommonJS exports.default value.
 * 
 * @param root Root folder to begin searching.
 * @param group Group object to bind to directory command.
 * @returns Group object
 */
export function cmddir<G extends Group<any, any, any, any, any>>(root: string, group: G): G
export function cmddir<G extends Group<any, any, any, any, any>>(root?: string, group?: G): G {
  if (!root) {
    root = '.'
  }
  if (!group) {
    group = new Group() as any
  }
  const files = readdirSync(root)
  let result: Group<any, any, any, any, any> = group as G
  for (let file of files) {
    try {
      file = file.replace(/\.(t|j)s$/, '')
      const plugin = require(resolve(root, file))?.default
      if (plugin instanceof Group) {
        result = result.addGroup(plugin.name(file))
      } else if (plugin instanceof Command) {
        result = result.addCommand(plugin.name(file) as any)
      }
    } catch { }
  }
  return result as G
}