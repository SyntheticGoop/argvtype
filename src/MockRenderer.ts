export * from 'ink-testing-library'
import { render } from 'ink-testing-library'

export class MockRenderer {
  readonly render: typeof render
  readonly result: Promise<ReturnType<typeof render>>
  constructor() {
    let cb: (result: ReturnType<typeof render>) => void
    this.result = new Promise(k => cb = k)
    this.render = (...args: Parameters<typeof render>) => {
      const r = render(...args)
      cb(r)
      return r
    }
  }
}