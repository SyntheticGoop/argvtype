import { CatchableError } from './Error'
import { UNSET } from './singletons/UNSET'

export class ArgumentErrorMultipleVariadic extends CatchableError {
  constructor() {
    super('There cannot be multiple Variadic Arguments')
  }
}
export class ArgumentErrorMultipleWithOptional extends CatchableError {
  constructor() {
    super('There cannot be Variadic Arguments with Optional Arguments')
  }
}

/**
 * Argument
 * 
 * Holds the individual argument schema.
 * 
 * An argument is an positional value which is passed to the command line.
 */
export class Argument<
  Name extends string,
  Desc extends string | null = null,
  Type = unknown,
  Multiple extends boolean = false,
  Optional extends boolean = false,
  TypeName extends string | null = null,
  Restrict extends string[] | null = null,
  > {

  /**
   * Create new Argument
   * 
   * @param name Unique name of argument.
   */
  constructor(
    name: Name,
  )
  /**
   * Create new Argument
   * 
   * @param name Unique name of argument.
   * @param description Short (1 line) description of the argument.
   * @param cast Function to cast input value to the output type. Throws class extending CatchableError on cast failure.
   * @param variadic Set to true to make the argument variadic.
   * @param optional Set to true to make the argument optional.
   * @param typename Set the type name of the argument variable.
   * @param enums Restrictive list of strings that the argument can accept.
   * @param defaults The default value of the flag.
   */
  constructor(
    name: Name,
    description: Desc,
    cast: ((input: Optional extends true ? (Restrict extends null ? string : Restrict extends Array<infer T> ? T : never) | null : (Restrict extends null ? string : Restrict extends Array<infer T> ? T : never)) => Type) | null,
    variadic: Multiple,
    optional: Optional,
    typename: TypeName,
    enums: Restrict,
    defaults: Type
  )
  constructor(
    private _name: Name,
    private _description: Desc = null as Desc,
    private _cast: ((input: Optional extends true ? (Restrict extends null ? string : Restrict extends Array<infer T> ? T : never) | null : (Restrict extends null ? string : Restrict extends Array<infer T> ? T : never)) => Type) | null = null as null,
    private _variadic: Multiple = false as Multiple,
    private _optional: Optional = false as Optional,
    private _typename: TypeName = null as TypeName,
    private _enum: Restrict = null as Restrict,
    private _defaults: Type | typeof UNSET = UNSET
  ) {
    this.coerce = this.coerce.bind(this)
    this.variadic = this.variadic.bind(this)
  }

  /**
   * Set the description of the argument.
   * 
   * @param description Short (1 line) description of the argument.
   */
  public description<Desc extends string | null>(description: Desc) {
    return new Argument(this._name, description, this._cast, this._variadic, this._optional, this._typename, this._enum, this._defaults)
  }

  /**
   * Set the allowed values of the argument.
   * 
   * @param restrict Restrictive list of strings that the argument can accept.
   */
  public enum<Restrict extends string[]>(...restrict: [...Restrict]) {
    return new Argument(this._name, this._description, this._cast as (input: Optional extends true ? (Restrict extends null ? string : Restrict extends Array<infer T> ? T : never) | null : (Restrict extends null ? string : Restrict extends Array<infer T> ? T : never)) => Restrict extends Array<infer T> ? T : never, this._variadic, this._optional, this._name, restrict, this._defaults as Restrict extends Array<infer T> ? T : never)
  }

  /**
   * Set the type, type name and casting function of the argument.
   * 
   * @param name Set the type name of the argument variable.
   * @param cast Function to cast the input value to the output type. Throws class extending CatchableError on cast failure.
   */
  public type<TypeName extends string | null, Type>(name: TypeName, cast: (input: Optional extends true ? (Restrict extends null ? string : Restrict extends Array<infer T> ? T : never) | null : (Restrict extends null ? string : Restrict extends Array<infer T> ? T : never)) => Type = x => x as unknown as Type) {
    return new Argument(this._name, this._description, cast, this._variadic, this._optional, name, this._enum, this._defaults as unknown as Type)
  }

  /**
   * Set the type and casting function of the argument.
   * 
   * @param cast Function to cast the input value to the output type. Throws class extending CatchableError on cast failure.
   */
  public coerce<Type>(cast: (input: Optional extends true ? (Restrict extends null ? string : Restrict extends Array<infer T> ? T : never) | null : (Restrict extends null ? string : Restrict extends Array<infer T> ? T : never)) => Type) {
    return new Argument(this._name, this._description, cast, this._variadic, this._optional, this._typename, this._enum, this._defaults as unknown as any)
  }

  /**
   * Set argument to be variadic.
   */
  public variadic(): Argument<Name, Desc, Type, true, Optional, TypeName, Restrict>
  /**
   * Set argument to be variadic.
   * 
   * @param variadic Set to true to make argument variadic.
   */
  public variadic<Multiple extends boolean>(variadic: Multiple): Argument<Name, Desc, Type, Multiple, Optional, TypeName, Restrict>
  public variadic<Multiple extends boolean>(variadic: Multiple = true as Multiple): Argument<Name, Desc, Type, Multiple, Optional, TypeName, Restrict> {
    return new Argument(this._name, this._description, this._cast, variadic, this._optional, this._typename, this._enum, this._defaults as Type)
  }

  /**
   * Set argument to be required.
   */
  public required(): Argument<Name, Desc, Type, Multiple, false, TypeName, Restrict>
  public required<Optional extends boolean>(optional: Optional = false as Optional): Argument<Name, Desc, Type, Multiple, Optional, TypeName, Restrict> {
    return new Argument(this._name, this._description, this._cast as unknown as (input: Optional extends true ? (Restrict extends null ? string : Restrict extends Array<infer T> ? T : never) | null : (Restrict extends null ? string : Restrict extends Array<infer T> ? T : never)) => Type, this._variadic, optional, this._typename, this._enum, this._defaults as Type)
  }

  /**
   * Set argument to be optional.
   */
  public optional(): Argument<Name, Desc, Type, Multiple, true, TypeName, Restrict>
  /**
   * Set argument optional state.
   * 
   * @param optional Set to true to make argument optional.
   */
  public optional<Optional extends boolean>(optional: Optional): Argument<Name, Desc, Type, Multiple, Optional, TypeName, Restrict>
  public optional<Optional extends boolean>(optional: Optional = true as Optional): Argument<Name, Desc, Type, Multiple, Optional, TypeName, Restrict> {
    return new Argument(this._name, this._description, this._cast as unknown as (input: Optional extends true ? (Restrict extends null ? string : Restrict extends Array<infer T> ? T : never) | null : (Restrict extends null ? string : Restrict extends Array<infer T> ? T : never)) => Type, this._variadic, optional, this._typename, this._enum, this._defaults as Type)
  }


  /**
   * Set the name of the argument.
   * 
   * @param name Unique name of argument.
   */
  public name<Name extends string>(name: Name) {
    return new Argument(name, this._description, this._cast, this._variadic, this._optional, this._typename, this._enum, this._defaults)
  }

  /**
   * Set the default value of the argument.
   * 
   * @param defaults The default value of the argument.
   */
  public default<DefaultType extends (unknown extends Type ? any : Type)>(defaults: DefaultType) {
    return new Argument(this._name, this._description, this._cast as unknown as (input: Optional extends true ? (Restrict extends null ? string : Restrict extends Array<infer T> ? T : never) | null : (Restrict extends null ? string : Restrict extends Array<infer T> ? T : never)) => Type, this._variadic, this._optional, this._typename, this._enum, defaults)
  }


  /** Argument Configuration */
  get $() {
    return {
      name: this._name,
      typename: this._typename,
      description: this._description,
      cast: this._cast,
      variadic: this._variadic,
      optional: this._optional,
      enum: this._enum,
      default: this._defaults
    }
  }
}


/**
 * Arguments
 * 
 * Holds entire positional argument schema.
 * 
 * Arguments contain all the positional arguments which can be passed to the command.
 */
export class Arguments<Args extends Array<Argument<any, any, any, any, any, any, any>> = [], KeyArgs extends Record<string, Argument<any, any, any, any, any, any, any>> = {}> {
  /** Create new Arguments */
  constructor()
  /**
   * Create new Arguments
   * 
   * @param args List of argument schemas.
   * @param keyargs Keyed list of argument schemas.
   */
  constructor(
    args: [...Args],
    keyargs: KeyArgs
  )
  constructor(
    private _arguments: [...Args] = [] as unknown as [...Args],
    private _keyargs: KeyArgs = {} as KeyArgs,
  ) { }

  /**
   * Add an argument to the argument list via a callback.
   * 
   * The callback will provide the argument object to chain the setup.
   * The returned value of the callback will be appended to the arguments.
   * 
   * @param name Name of argument.
   * @param arg Argument creation callback function.
   */
  public arg<Name extends string, Arg extends Argument<Name, any, any, any, any, any, any>>(name: Name, arg: ((arg: Argument<Name>) => Arg)) {
    return this.addArg(arg(new Argument(name)))
  }


  /**
   * Add an argument to the argument list.
   * 
   * @param arg Argument to add.
   */
  public addArg<Name extends string, Arg extends Argument<Name, any, any, any, any, any, any>>(arg: Arg) {
    if (arg.$.variadic && this._arguments.some(x => x.$.variadic)) throw new ArgumentErrorMultipleVariadic()
    if (arg.$.variadic && this._arguments.some(x => x.$.optional)) throw new ArgumentErrorMultipleWithOptional()
    return new Arguments([...this._arguments, arg], { ...this._keyargs, [arg.$.name]: arg } as KeyArgs & { [K in typeof arg extends Argument<infer T, any, any, any, any, any, any> ? T : never]: typeof arg })
  }

  /** Arguments list */
  get $() {
    return {
      args: this._arguments,
      keyargs: this._keyargs
    }
  }
}