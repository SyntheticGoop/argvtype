import { render } from 'ink'
import { argparse } from './argparse'
import { Command } from './Command'
import { CatchableError } from './Error'
import { Group } from './Group'
import { help } from './help'

export class CliNoCommandSelected extends CatchableError {
  constructor() {
    super('No command was selected')
  }
}

/**
 * Parse the argument list and execute the action from the command schema.
 * 
 * if --help is one of the arguments, the interactive help menu will be opened.
 * 
 * @param args Input argument list.
 * @param cmd Command schema.
 */
export async function cli<CMD extends Group<any, any, any, any, any> | Command<any, any, any, any, any, any, any>>(args: string[], cmd: CMD): Promise<unknown>
/**
 * Parse the argument list and execute the action from the command schema.
 * 
 * Set the helpflag value to override the help menu flag detection from the default of '--help' to something else.
 * The help option will be triggered whenever an argument contains this value exactly.
 * 
 * @param args Input argument list.
 * @param cmd Command schema.
 * @param helpflag Help argument matching string.
 */
export async function cli<CMD extends Group<any, any, any, any, any> | Command<any, any, any, any, any, any, any>>(args: string[], cmd: CMD, helpflag: string): Promise<unknown>
export async function cli<CMD extends Group<any, any, any, any, any> | Command<any, any, any, any, any, any, any>>(this: { renderer?: typeof render, noexit?: boolean, log?: (...args: any[]) => void } | void, args: string[], cmd: CMD, helpflag: string = '--help'): Promise<unknown> {
  const cmdstack: string[] = []
  let active: CMD = cmd
  for (const arg of args.filter(x => x !== helpflag)) {
    if (active instanceof Command) break
    const next = (active as Group<any, any, any, any, any>).$.commands[arg]
    if (!next) break
    active = next
    cmdstack.push(arg)
  }

  if (args.includes(helpflag)) {
    await (await help(false, cmd instanceof Command ? cmd.$ : cmd.$, cmdstack, this ? this.renderer : undefined))?.waitUntilExit?.()
    if (this && this.noexit) {
      return
    } else {
      process.exit(0)
    }
  }

  try {
    if (active instanceof Group) throw new CliNoCommandSelected()
    const parsed = argparse(args.slice(cmdstack.length), active as any)
    return await (active as any).$.action(parsed)
  } catch (e: unknown) {
    await help(true, cmd.$, cmdstack, this ? this.renderer : undefined)
    if (e instanceof CatchableError) {
      (this ? this.log ?? console.log : console.log)(e.message)
      if (this && this.noexit) {
        return
      } else {
        process.exit(1)
      }
    } else {
      throw e
    }
  }
}